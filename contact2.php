<?php

include 'init.php';

$captcha = load('fcaptcha');

$email = $_POST['email'] ?? NULL;
if (is_null($email))
    redirect(DEFAULT_URL);

if (!$captcha->validate($_POST['ruhuman'], 'captcha')) {
    unset($_SESSION['captcha']);
    redirect('contact.php');
}
unset($_SESSION['captcha']);

$emails = $user->get_admin_emails();
foreach ($emails as $email) {

    $content = "\nName: {$_POST['name']}\n";
    $content .= "Email: {$_POST['email']}\n";
    $content .= "Message:\n\n";
    $content .= $_POST['message'] . "\n\n";

    $result = mail($email, $cfg['blog_title'] . ' Contact Form', $content);
}

emsg('S', 'Your contact message was sent');

redirect('contact.php');


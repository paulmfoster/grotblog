<?php

include 'init.php';

access(0, DEFAULT_URL);
$artno = $_POST['artno'] ?? NULL;
if (is_null($artno)) {
    redirect(ADMIN_URL);
}
$articles = model('articles', $db);
$articles->delete_article($artno);
redirect(ADMIN_URL);


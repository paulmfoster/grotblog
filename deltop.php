<?php

include 'init.php';

$topicid = $_GET['id'] ?? NULL;
if (is_null($topicid))
    redirect(ADMIN_URL);

access(0, DEFAULT_URL);

$topics = model('topics', $db);

$fields = array(
    'topicid' => array(
        'name' => 'topicid',
        'type' => 'hidden',
        'value' => $topicid
    ),
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Delete'
    )
);
$form->set($fields);

$topic = $topics->get_topic_from_id($topicid);	
if (!$topic) {
    // topic is non-existent
    emsg('F', 'Cannot delete a non-existent topic');
    redirect(ADMIN_URL);
}

$page_title = 'Delete Topic';
$return = 'deltop2.php';
include VIEWDIR . 'deltop.view.php';


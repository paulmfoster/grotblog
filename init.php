<?php

// define system directories
define('INCDIR', 'includes/');
define('LIBDIR', 'libraries/');
define('CFGDIR', 'config/');
define('PRINTDIR', 'printq/');
define('IMGDIR', 'images/');
define('DATADIR', 'data/');
define('MODELDIR', 'models/');
define('VIEWDIR', 'views/');
define('ARTSDIR', 'articles/');
define('FILESDIR', 'files/');
define('DEFAULT_URL', 'index.php');
define('ADMIN_URL', 'admin.php');

// provide common utilities
include INCDIR . 'utils.inc.php';

session_start();

$cfg = parse_ini_file(CFGDIR . 'config.ini');
$dsn = explode(':', $cfg['dsn']);
$present = file_exists($dsn[1]);
$db = load('database', $cfg['dsn']);
if (!$present) {
    genpop($db, 'coldstart.php');
}

load('errors');
load('messages');
$form = load('form');
load('xdate');
load('sanitize');
load('validate');
include INCDIR . 'pager.inc.php';
include LIBDIR . 'blognav.lib.php';

$user = load('user', $db);
$nav = new blognav($db, $user);

function access($level, $url = NULL)
{
	global $user;

	$okay = $user->access($level);
	if (!$okay) {
        if (is_null($url)) {
            header('Location: ' . DEFAULT_URL);
        }
        else {
    		header('Location: ' . $url);
        }
		exit();
	}
}

/**
 * explode_timestamp()
 *
 * Take the database timestamp and turn it into
 * a format which can be displayed on screen
 *
 * @param string $timestamp The timestamp
 *
 * @return string The reformatted timestamp
 *
 */

function explode_timestamp($timestamp)
{
    $month_names = [
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    ];

    $yr = substr($timestamp, 0, 4);
    $mo = substr($timestamp, 4, 2);
    $da = substr($timestamp, 6, 2);
    $hr = substr($timestamp, 8, 2);
    $min = substr($timestamp, 10, 2);
    $sec = substr($timestamp, 12, 2);

    $date = sprintf('%s %s %s %s:%s:%s', $da, $month_names[$mo], $yr, $hr, $min, $sec);
    return $date;
}


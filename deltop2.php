<?php

include 'init.php';

$topicid = $_POST['topicid'] ?? NULL;
if (is_null($topicid)) {
    redirect(ADMIN_URL);
}

$topics = model('topics', $db);

if ($topics->delete_topic($_POST['topicid'])) {
    emsg('S', 'Topic successfully deleted');
}

redirect(ADMIN_URL);

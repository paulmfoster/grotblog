<?php
$schema = [
'PRAGMA foreign_keys=OFF',
'BEGIN TRANSACTION',
"CREATE TABLE periods (id integer primary key autoincrement, period varchar(6), qty integer)",
"CREATE TABLE topics (id integer primary key autoincrement, abbrev varchar(10), name varchar(50), qty integer default 0)",
"CREATE TABLE art2tops (id integer primary key autoincrement, artno integer references articles(id), topicid integer references topics(id))",
"CREATE TABLE articles (id integer primary key autoincrement, author integer references user(id), filename varchar(255) not null, timestamp varchar(14) not null, comments_allowed boolean default 0, title varchar(255) not null, teaser varchar(255), markup varchar(4), qty integer default 0)",
"CREATE TABLE user (id integer primary key autoincrement, login varchar(30), password varchar(255), name varchar(50) not null, email varchar(255) not null, nonce varchar(255) not null, level integer default 255)",
"CREATE TABLE confirm (id integer primary key autoincrement, login varchar(30), password varchar(255), name varchar(50), email varchar(255), nonce varchar(255), level integer, ip varchar(15), link varchar(255), timestamp integer)",
'COMMIT'
];

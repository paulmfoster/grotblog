<?php

include 'init.php';

$tsr = model('teasers', $db);

$page = $_GET['page'] ?? 1;
$author = $_GET['author'] ?? NULL;
$period = $_GET['period'] ?? NULL;
$topic = $_GET['topic'] ?? NULL;


if (!is_null($author)) {
    $teasers = $tsr->get('a', $author, $page);
    $pager = $tsr->pager('a', $author, $page, 'showtsr.php?author=' . $author . '&page=', $page);
}
elseif (!is_null($period)) {
    $teasers = $tsr->get('p', $period, $page);
    $pager = $tsr->pager('p', $period, $page, 'showtsr.php?period=' . $period . '&page=', $page);
}
elseif (!is_null($topic)) {
    $teasers = $tsr->get('t', $topic, $page);
    $pager = $tsr->pager('t', $topic, $page, 'showtsr.php?topic=' . $topic . '&page=', $page);
}
else {
    $teasers = $tsr->get('x', NULL, $page);
    $pager = $tsr->pager('x', NULL, $page, 'showtsr.php?page=', $page);
}

if ($teasers == FALSE) {
    $view = 'no_articles.view.php';
}
else {
    $view = 'teasers.view.php';
}

include VIEWDIR . $view;

<?php
 
include 'init.php';

$artno = $_GET['id'] ?? NULL;
if (is_null($artno))
    redirect(ADMIN_URL);

$articles = model('articles', $db);

if (is_null($artno)) {
    redirect(ADMIN_URL);
}

access(0, DEFAULT_URL);

$fields = array(
    'artno' => array(
        'name' => 'artno',
        'type' => 'hidden',
        'value' => $artno
    ),
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Delete'
    )
);

$form->set($fields);

$article = $articles->get_article($artno);
$page_title = 'Delete Article';
$return = 'delart2.php';
include VIEWDIR . 'delart.view.php';

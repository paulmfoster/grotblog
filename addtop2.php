<?php

// Add topic, part 2

include 'init.php';

$topics = model('topics', $db);

access(0, DEFAULT_URL);
if ($topics->add_topic($_POST)) {
    emsg('S', 'Topic successfully added');
}
redirect(ADMIN_URL);


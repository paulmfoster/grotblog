<?php

include 'init.php';

$login = $_POST['login'] ?? NULL;
if (is_null($login)) {
    redirect(DEFAULT_URL);
}

if ($user->login($_POST)) {
    emsg('S', 'You are now logged in');
    if ($user->is_admin()) {
        redirect('admin.php');
    }
}
else {
    emsg('F', 'Login FAILED');
}

redirect(DEFAULT_URL);

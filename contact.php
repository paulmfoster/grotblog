<?php

include 'init.php';

$captcha = load('fcaptcha');

if (isset($_SESSION['user'])) {
    $auser = $user->get_user_by_nonce($_SESSION['user']);
}
else {
    $auser = array(
        'name' => '',
        'email' => ''
    );
}

$fields = array(
    'name' => array(
        'name' => 'name',
        'type' => 'text',
        'size' => 40,
        'required' => 1,
        'class' => 'required',
        'maxlength' => 70,
        'value' => $auser['name']
    ),
    'email' => array(
        'name' => 'email',
        'type' => 'text',
        'size' => 40,
        'required' => 1,
        'class' => 'required',
        'maxlength' => 255,
        'value' => $auser['email']
    ),
    'message' => array(
        'name' => 'message',
        'type' => 'textarea',
        'rows' => 10,
        'cols' => 40,
        'required' => 1,
        'class' => 'required',
        'wrap' => 'soft'
    ),
    'ruhuman' => array(
        'name' => 'ruhuman',
        'type' => 'text',
        'size' => 20,
        'maxlength' => 20,
        'required' => 1,
        'class' => 'required'
    ),
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Send'
    )
);

$form->set($fields);

$code = $captcha->get_captcha(6);
$captcha->set_session('captcha', $code);

$page_title = 'Contact';
$return = 'contact2.php';

include VIEWDIR . 'contact.view.php';


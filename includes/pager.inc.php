<?php


/**
 * pager()
 *
 * Create a "pager" table for blog pages
 *
 * NOTE: the $pager_width parameter must be an odd number.
 * Even numbers will work, but under certain circumstances,
 * the pager table will be wider than specified by one cell.
 *
 * @param integer $total_posts
 * @param integer $posts_per_page
 * @param integer $current_pageno
 * @param integer $pager_width (basic number of cells in pager table)
 * @param string $pager_url Basic URL for a page; we add pageno to this
 *
 * @return string The HTML text of the pager table
 *
 */

function pager($total_posts, $posts_per_page, $current_pageno, $pager_width, $pager_url) 
{
	$remainder = $total_posts % $posts_per_page;

    // calculate total pages
	if ($remainder != 0) {
		$total_pages = intdiv($total_posts, $posts_per_page) + 1;
    }
	else {
		$total_pages = intdiv($total_posts, $posts_per_page);
    }

    // calculate total slots
    if ($total_pages >= $pager_width) {
        $total_slots = $pager_width;
    }
    else {
        $total_slots = $total_pages;
    }

	// build full pager array, not all of which will actually show
	$pager = array();
	for ($i = 0; $i < $total_pages; $i++) {
		$pager[$i] = array('txt' => $i + 1, 'url' => $pager_url . ($i + 1));
	}

	// figure out the number of slots before and after
	// *centered* current page

	$left_bias = floor($total_slots / 2);
	$right_bias = floor($total_slots / 2);

	// set preliminary values for first and last pages
	
	$first_page = $current_pageno - $left_bias;
	$last_page = $current_pageno + $right_bias;
	if ($first_page < 1) {
		$first_page = 1;
		$last_page = $total_slots;
	}
	elseif ($last_page > $total_pages) {
		$last_page = $total_pages;
		$first_page = $total_pages - $total_slots + 1;
	}

	// beginning of pager table

	$str = '<div id="pager">' . PHP_EOL;
    $str .= '<strong>' . PHP_EOL;
	$str .= 'PAGE&nbsp;' . PHP_EOL;

	// in which we draw the pager table cells

	for ($k = $first_page; $k <= $last_page; $k++) {

		// before the first page cell, add a "<" if applicable

		if ($k == $first_page && $current_pageno > $first_page) {
			$str .= '<a href="' . $pager[$current_pageno - 2]['url'] . '">';
			$str .= '&lt;</a>' . PHP_EOL;
		}

		// draw non-current page cells

		if ($k != $current_pageno) {
			$str .= '<a href="' . $pager[$k - 1]['url'] . '">';
			$str .= $pager[$k - 1]['txt'] . '</a>' . PHP_EOL;
		}
		else {
			// draw the current page cell (no link)
			$str .= $k . PHP_EOL;
		}

		// draw a right hand ">" if applicable
		if ($k == $last_page && $current_pageno < $last_page) {
			$str .= '<a href="' . $pager[$current_pageno]['url'] . '">';
			$str .= '&gt;</a>' . PHP_EOL;
		}
	}

	// finish up the table
	
    $str .= '</strong>' . PHP_EOL;
	$str .= '</div>'; // end of pager class div

	return $str;
}


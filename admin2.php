<?php

include 'init.php';

if (isset($_POST['artdel'])) {
    redirect('delart.php?id=' . $_POST['artno']);
}
elseif (isset($_POST['topedt'])) {
    redirect('edittop.php?id=' . $_POST['topicid']);
}
elseif (isset($_POST['topdel'])) {
    redirect('deltop.php?id=' . $_POST['topicid']);
}
elseif (isset($_POST['usredt'])) {
    redirect('edituser.php?id=' . $_POST['userid']);
}
elseif (isset($_POST['usrdel'])) {
    redirect('deluser.php?id=' . $_POST['userid']);
}
elseif (isset($_POST['cfgedt'])) {
    redirect('editcfg.php');
}

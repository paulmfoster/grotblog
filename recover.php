<?php

// Situation: For some reason (like your blog being taken down), you
// want to restart your blog with a new hosting company.
//
// Solution: Reinstall Grotblog software, along with your article files and
// ini files in the articles/ directory. Then run this program. It will
// rebuild your database.
//
// Once completed, you should login as admin using the password
// "password". Then change the admin password. Then login as your old
// login, using the password "password". Now change the user name and
// password. You might also want to change the names of the different
// categories.

include 'init.php';

access(0, DEFAULT_URL);

function get_user_by_login($login)
{
	global $db;

	$sql = "SELECT * FROM user WHERE login = '$login'";
	$user_rec = $db->query($sql)->fetch();
	return $user_rec;
}

function get_topic_by_abbrev($abbrev)
{
	global $db;

	$sql = "SELECT * FROM topics WHERE abbrev = '$abbrev'";
	$topics_rec = $db->query($sql)->fetch();
	if ($topics_rec !== FALSE) {
		return $topics_rec;
	}
	return FALSE;
}

function remove_extension($filename)
{
	$posn = strpos($filename, '.');
	$base = substr($filename, 0, $posn);
	return $base;
}

/**
 * Parse the metadata from a file.
 *
 * The metadata of an article file sits inside a set of HTML comment tags
 * (<!--, -->). We feed this routine the whole file, and return an array of
 * the values found. In the process, we must parse this data.
 *
 * @param array all lines of a file
 * @return array metadata array
 */

// file(filename, FILE_IGNORE_NEW_LINES); // must add them back later

function parse_metadata($lines)
{
    $meta = [
        'title' => '',
        'login' => '',
        'markup' => '',
        'timestamp' => '',
        'topics' => '',
        'teaser' => ''
    ];

    foreach ($lines as $line) {
        $start = substr($line, 0, 4);
        switch ($start) {
        case '<!--':
            break;
        case 'titl':
            $arr = parse_ini_string($line);
            $meta['title'] = $arr['title'];
            break;
        case 'logi';
            $arr = parse_ini_string($line);
            $meta['login'] = $arr['login'];
            break;
        case 'mark':
            $arr = parse_ini_string($line);
            $meta['markup'] = $arr['markup'];
            break;
        case 'time':
            $arr = parse_ini_string($line);
            $meta['timestamp'] = $arr['timestamp'];
            break;
        case 'topi':
            $arr = parse_ini_string($line);
            $meta['topics'] = $arr['topics'];
            break;
        case 'teas':
            $arr = parse_ini_string($line);
            $meta['teaser'] = $arr['teaser'];
            break;
        case '-->':
            break(2);
        }
    }

    return $meta;
}

/////////////////////////////////////////////////////////////////////

// Reconstructing blog

// kill the old database file
if (file_exists(DATADIR . 'blog.sq3')) {
	unlink(DATADIR . 'blog.sq3');
    $db = NULL;
}

// Creating tables

$dsn = explode(':', $cfg['dsn']);
$present = file_exists($dsn[1]);
$db = new database($cfg['dsn']);
if (!$present) {
    genpop($db, 'coldstart.php');
}

// Creating admin user

$admin = [
	'id' => 1,
	'login' => 'admin',
	'password' => password_hash('password', PASSWORD_BCRYPT), // placeholder for now
	'name' => 'Administrator',
	'email' => 'me@example.com',
	'nonce' => md5('admin'),
	'level' => 0
];
$db->insert('user', $admin);

// Scanning article files and populating tables

// scan article files
$handle = opendir(ARTSDIR); 
while (FALSE !== ($file = readdir($handle))) {
	if ($file == '.' || $file == '..' || $file == '.gitkeep') {
		continue;
    }

    $lines = file(ARTSDIR . $file, FILE_IGNORE_NEW_LINES);
    $meta = parse_metadata($lines);

    // we no longer need the original file contents here

    // update user table as needed

    $usr = get_user_by_login($meta['login']);
    if ($usr === FALSE) {
        $uid = $db->lastid('user') + 1;
        $rec = array(
            'id' => $uid,
            'login' => $meta['login'],
            'password' => password_hash('password', PASSWORD_BCRYPT), // placeholder for now
            'email' => 'me@example.com',
            'name' => $meta['login'], // placeholder for now
            'nonce' => md5($meta['login']),
            'level' => 0
        );
        $db->insert('user', $rec);
    }
    else {
        $uid = $usr['id'];
    }

    // update articles table

    $articles_rec = array(
        'author' => $uid,
        'filename' => $file,
        'timestamp' => $meta['timestamp'],
        'title' => $meta['title'],
        'teaser' => $meta['teaser'],
        'markup' => $meta['markup'],
        'qty' => 0
    );

    $db->insert('articles', $articles_rec);
    $artno = $db->lastid('articles');

    // update periods table

    $period = substr($meta['timestamp'], 0, 6);

    $sql = "SELECT qty FROM periods WHERE period = '$period'";
    $qty = $db->query($sql)->fetch();
    if ($qty == FALSE) {
        $db->insert('periods', array('period' => $period, 'qty' => 1));
    }
    else {
        $sql = "UPDATE periods SET qty = qty + 1 WHERE period = '$period'";
        $db->query($sql);
    }

    // update topics and art2tops

    $topics_abbr = explode(',', $meta['topics']);
    foreach ($topics_abbr as $ta) {
        $top = get_topic_by_abbrev($ta);
        if ($top === FALSE) {
            $rec = array(
                'abbrev' => $ta,
                'name' => ucfirst($ta),
                'qty' => 1
            );
            $db->insert('topics', $rec);
            $tid = $db->lastid('topics');
        }
        else {
            $db->update('topics', ['qty' => $top['qty'] + 1], "id = {$top['id']}");
            $tid = $top['id'];
        }
        $db->insert('art2tops', ['artno' => $artno, 'topicid' => $tid]);
    }

}

// Task completed

emsg('S', 'Successfully reconstructed the blog from existing posts. Be sure to change passwords and email addresses.');
redirect(DEFAULT_URL);


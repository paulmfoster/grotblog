<?php

// code to migrate 4.3 articles/database to 5.0.

include 'init.php';

$path = 'articles/';
$meta_path = 'meta/';

function change_name($file)
{
	$file = str_replace('-', ' ', $file);
	$file = ucwords($file);
	$file = str_replace(' ', '_', $file);
	return $file;
}

function parse_file($file)
{
	global $user, $cfg, $blog;

	$errors = 0;

	// set up blank record
	$article = [
		'title' => '',
		'teaser' => '',
		'comments_allowed' => 0,
		'markup' => '',
		'topics_str' => '',
		'login' => '',
		'timestamp' => '',
		'content' => ''
	];

	$lines = explode("\n", $file);

	$in_teaser = FALSE;
	$in_content = FALSE;

	foreach ($lines as $line) {
		// remove extraneous characters
		$line = trim($line);
		if (!$in_teaser && !$in_content) {
			$tag = substr($line, 0, 6);
			switch ($tag) {
			case '@title':
				$article['title'] = substr($line, 7);
				break;
			case '@autho':
				$article['login'] = substr($line, 8);
				break;
			case '@marku':
				$article['markup'] = substr($line, 8);
				break;
			case '@times':
				$article['timestamp'] = substr($line, 11);
				break;
			case '@comme':
				$article['comments_allowed'] = 1;
				break;
			case '@topic':
				$article['topics_str'] = substr($line, 8);
				break;
			case '@tease':
				$in_teaser = TRUE;
				break;
			}
		}
		elseif ($in_teaser) {
			if (substr($line, 0, 6) != '@conte') {
				$article['teaser'] .= $line . "\n";
			}
			else {
				$in_teaser = FALSE;
				$in_content = TRUE;
			}
		}
		elseif ($in_content) {
			$article['content'] .= $line . "\n";
		}
	}

	// title

	if (empty($article['title'])) {
		$errors++;
		emsg('F', "Article has no title ($file)");
	}

	// author

	if (empty($article['login'])) {
		$errors++;
		emsg('F', "No author supplied ($file)");
	}

	// markup

	if (empty($article['markup'])) {
		$errors++;
		emsg('F', "Markup not properly specified ($file)");
	}
	elseif ($article['markup'] != 'md' && $article['markup'] != 'html') {
		$errors++;
		emsg('F', "Invalid markup specification supplied ($file)");
	}

	// timestamp

	if (empty($article['timestamp'])) {
		$errors++;
		emsg('F', "Invalid timestamp supplied ($file)");
	}

	// topics

	if (empty($article['topics_str'])) {
		$errors++;
		emsg('F', "No topics supplied ($file)");
	}

	// teaser

	if (empty($article['teaser'])) {
		$article['teaser'] = substr($article['content'], 0, $cfg['teaser_chars']);
	}

	// final
	if ($errors == 0) {
		return $article;
	}

	// something went wrong
	return FALSE;
}

// loop through all the files

if ($handle = opendir($path)) {
    while ($file = readdir($handle)) {
		if ('.' === $file)
			continue;
		if ('..' === $file)
			continue;

		$new_filename = change_name($file);
		echo "<hr/>Processing: $file -> $new_filename<br>";

		$sql = "SELECT id FROM articles WHERE filename = '$file'";
		$rec = $db->query($sql)->fetch();
		if (empty($rec)) {
			emsg('F', "Can't find that database record ($file)");
		}
		$contents = file_get_contents("articles/$file");
		$article = parse_file($contents);

		$meta = "title = \"${article['title']}\"\n";
		$meta .= "login = \"${article['login']}\"\n";
		$meta .= "markup = ${article['markup']}\n";
		$meta .= "timestamp = \"${article['timestamp']}\"\n";
		$meta .= "comments_allowed = ${article['comments_allowed']}\n";
		$meta .= "topics = \"${article['topics_str']}\"\n";

		$article['teaser'] = str_replace("\n", ' ', $article['teaser']);
		$article['teaser'] = rtrim($article['teaser']);
		$article['teaser'] = str_replace('"', '\"', $article['teaser']);
		$meta .= "teaser = \"${article['teaser']}\"\n";

		// write ini file
		file_put_contents("${meta_path}${new_filename}.ini", $meta);

		// write new article file
		file_put_contents("${path}${new_filename}.${article['markup']}", $article['content']);

		// delete old article file
		unlink("${path}${file}");

		// update database record
		$data = ['filename' => "$new_filename.${article['markup']}"];
		$db->update('articles', $data, "filename = '$file'");

		show_messages();

    }
    closedir($handle);

}
else {
	echo "Can't file directory $path.";
}

echo "<h1>DONE!</h1>";


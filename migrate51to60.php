<?php

// define system directories
define('SYSDIR', 'system/');
define('INCDIR', SYSDIR . 'includes/');
define('LIBDIR', SYSDIR . 'libraries/');

// define application directories
define('APPDIR', 'app/');
define('CFGDIR', APPDIR . 'config/');
define('PRINTDIR', APPDIR . 'printq/');
define('IMGDIR', APPDIR . 'images/');
define('DATADIR', APPDIR . 'data/');
define('MODELDIR', APPDIR . 'models/');
define('VIEWDIR', APPDIR . 'views/');
define('CTLDIR', APPDIR . 'controllers/');
define('GINCDIR', APPDIR . 'includes/');
define('GLIBDIR', APPDIR . 'libraries/');

// provide common utilities
include INCDIR . 'utils.inc.php';

// add some utility code
load('errors');
load('messages');

// wipe out the existing database file
if (file_exists(DATADIR . 'blog.sq3')) {
    echo 'Deleting existing database file.<br/>';
    unlink(DATADIR . 'blog.sq3');
}

// make new database and tables
echo 'Creating new database file.<br/>';
$dsn = 'sqlite:' . DATADIR . 'blog.sq3';
$dest = make_tables($dsn, APPDIR . 'coldstart.sqlite');

echo 'Connecting to old database.<br/>';
$dsn = 'sqlite:' . 'data/blog.sq3';
$src = new database($dsn);

echo 'Populating the articles table.<br/>';
$sql = "select a.*, t.text as teaser, t.markup as markup, 0 as qty from articles as a join teasers as t on (t.artno = a.id)";
$articles = $src->query($sql)->fetch_all();
foreach ($articles as $art) {
    $dest->insert('articles', $art);
}

echo 'Populating topics table.<br/>';
$sql = "SELECT * FROM topics";
$topics = $src->query($sql)->fetch_all();
foreach ($topics as $top) {
    $dest->insert('topics', $top);
}

echo 'Populating art2topics table.<br/>';
$sql = "SELECT * FROM art2topics";
$art2topics = $src->query($sql)->fetch_all();
foreach ($art2topics as $a2p) {
    $dest->insert('art2topics', $a2p);
}

echo 'Populating periods table.<br/>';
$sql = "SELECT * FROM periods";
$periods = $src->query($sql)->fetch_all();
foreach ($periods as $per) {
    $dest->insert('periods', $per);
}

echo 'Skipping comments table.<br/>';

echo 'Setting up user database.<br/>';
echo 'Deleting old users database, if any.<br/>';
if (file_exists(DATADIR . 'users.sq3')) {
    unlink(DATADIR . 'users.sq3');
}

echo 'Creating the initial users database.<br/>';
load('user');
$dsn = "sqlite:" . DATADIR . 'users.sq3';
$user = new database($dsn);
// delete sample admin user
$sql = "DELETE FROM users";
$user->query($sql);

echo 'Populating users table.<br/>';
$sql = "SELECT * FROM user";
$usrs = $src->query($sql)->fetch_all();
foreach ($usrs as $usr) {
    $user->insert('users', $usr);
}

echo 'Populating confirm table.<br/>';
$sql = "SELECT * FROM confirm";
$cfms = $src->query($sql)->fetch_all();
if ($cfms !== FALSE) {
    foreach ($cfms as $cfm) {
        $user->insert('confirm', $cfm);
    }
}

echo 'DONE!';


<?php

include 'init.php';

$articles = model('articles', $db);

if (isset($_POST)) {
    $articles->add_article($_POST, $_FILES);
}
redirect(ADMIN_URL);


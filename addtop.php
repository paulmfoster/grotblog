<?php

// Add topic

include 'init.php';

access(0, DEFAULT_URL);

$fields = array(
    'abbrev' => array(
        'name' => 'abbrev',
        'type' => 'text',
        'size' => 10,
        'maxlength' => 10,
        'required' => 1
    ),
    'name' => array(
        'name' => 'name',
        'type' => 'text',
        'size' => 30,
        'maxlength' => 50,
        'required' => 1
    ),
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Add'
    )
);

$form->set($fields);
$page_title = 'Add Topic';
$return = 'addtop2.php';
include VIEWDIR . 'addtop.view.php';


# Grotblog

## What is this?

This is blog software for people who want to edit their blog entries
locally and upload them to their blog site. It is for people who are
relatively web-tech savvy.

## The Name

There was a 1970s television show from England called *The Fall and Rise
of Reginald Perrin*. In its second season, Reginald opens a shop called
"Grot" which sells useless products. While it ought to fail, the humor
is in the fact that, despite his best efforts to sabotage it, it
succeeds wildly. This was my first introduction to the word "grot",
which apparently is English slang for rubbish. I loved the series, and
decided to use the word in the name of my blog software. Just for fun.
Programmers often have a perverse and ironic sense of humor.

## Things I Dislike About Other Blog Software

I don't like the editors in most blog software. Those are where you
enter your blog entry. You have to do it online, and the editors are
laggy and, in my opinion, crappy. I want to compose my blog entries in
Vim. So my idea is that I compose my entries on my local computer and
upload them instead of editing them online.

I don't care about prior revisions of my entries. I don't know if blog
software typically tracks prior versions of the same entry, but I do
know that Wordpress's database structure make allowances for different
versions of an entry. I couldn't care less. If I want to track different
versions of what I write, I'll put the whole thing under git management.

Typically, blog software uses a database to store blog entries. I
consider this database abuse. SQL RDBMSes were never made to store an
unlimited amount of text in a field. They can now do it, but I consider
it a botch. There's no reason to drag around kilobytes of text in a
database. Just put the blog entry in a file, and reference the filename
in the database.

A lot of blog software displays a single entry (the latest), and
sometimes the next and the next as you scroll downwards. Alternatively,
they just show the whole current blog entry, and then you have to click
on an "Older Entries" link to get to the prior one. I don't want to have
to scoll down and click endless links to get to an entry I want to read.

In fact, I don't want to have to read through any entry. I want to see a
summary or "teaser" about the current entry to see if I even want to
read it. I'd like a whole page of teasers. If I want to read an entry, I
want to click on the teaser to read the whole entry.

Most blog software doesn't allow you to categorize blog entries. I write
on a variety of subjects. Someone may have no interest in my programming
material, but want to know what I have to say about movies. Blog entries
should be categorized by topic.

When I worked with Wordpress, I had to deal with the nightmare of
backing up my blog. They have a weak provision for backing up your
entries in an XML file. But it's only useful when you feed it to another
instance of Wordpress. In other words, it's a Wordpress backup for use
internally by Wordpress. I want my backup to be useful as a real backup.
What if I move from Wordpress to Drupal? That backup is useless.

Expanding on backups, how about if there is no backup? How about if I
just keep a copy of every entry on my local computer? And how about
having a way to reconstruct the blog from my local backups?

## Other Concerns

In the current social/political culture (summer 2020), it is possible to
have your blog content removed because someone was offended by it. If
you have a backup of the content, you're 70% of the way to restoring
your blog. But the content is attached to titles, topics, time periods,
etc. If you don't have a backup of that, you're looking at a long slog
to restore your blog. This software was designed to give you the ability
to recover your blog if it is cancelled.

## Features

### Editing/Uploading

There is no editor in Grotblog, as there is in most blog software.
Instead, you upload your article. You use whatever local editor you like,
and upload pages formatted in markdown or HTML. The link to upload blog
entries is on the Admin page.

### Revisions/Versioning

Grotblog does not track different versions of the same article. A
revised article simply replaces an older one. If you wish to track older
versions of an article, it is advised that you place articles under
version control wherever you keep your backups. You do have backups of
all your articles, right?

### Multiple Authors

Grotblog allows you to create, edit or delete multiple authors. All
authors have admin privileges. Only administrative level users can
create other users. Casual users may create accounts on your site, but
do not have administrative privileges. I'm not sure what benefit it
provides to register with your site, but I've provided it anyway.

### Comments

At one time I provided for comments, but I've removed this functionality.
I'm not particularly interested in discussions by other people of my posts,
Moreover, a comment system needs to be able to nest comments, and the
programming for that is more complicated than I care to tackle, for the
supposed benefits.

### Topics

In Grotblog, each article has one or more topics. Topics may be created,
edited and deleted. This allows users to seek articles on a specific
subject.

### Searching

There is currently no search feature in Grotblog. However, users may
view articles confined to a specific author, topic or time period.

### Teasers

Unlike other blog software, Grotblog does not show full articles on its
main page. Instead, it shows "teasers". Teasers are a few lines of
text which summarize its contents or encourage the user to read the
article. Once the user finds an article they wish to read, they may
click the "Read more" link to read the entire article.

### Configuration

You are allowed a page to directly edit the configuration. This allows you
to change various values, like

- Blog title
- Blog slogan (appears below the title)
- Number of characters allowed in a teaser
- Whether articles are shown chronologically or in reverse
- Number of article teasers per page
- The blog language

Technically, you may edit any other values in the configuration (there are
quite a few). You can also break the software by doing so. The above are
those recommended for editing.

### Styling

Grotblog comes to you with a very bland style. There is no way in Grotblog
currently to change this; there are no "templates". To change the styling,
you are encouraged to edit the `views/style.css` and/or the
`views/colors.css` files. These are fairly straightforward if you
understand CSS. Grotblog does not contain "templates". If you need
templates, I suggest Wordpress, which has a great many.

If you really want to hack away at the styling, the primary "framework" for
pages is in the `views/head.view.php` file. You can also hack the
`views/footer.view.php` file.

## Navigation

The left hand side of the page contains some links to "static" pages. These
pages are typcally HTML pages, and are held in the `views/` directory.
There is an **About** page and a **Policy** page. You may edit these files
in the views directory.

The left hand navigation links are governed by a file called
`leftnav.php`. It consists of a PHP array of links. It doesn't required
a deep knowledge of PHP to edit it. Just do entries like they are already
in the file. If you create a static file, it's suggested you do it as I
have, using the

```
'Title' => 'static.php?page=blah.html'
```

paradigm, and store the file in the `views/` directory. You may add
subcategories, links to your resume, your other websites, etc.

## Requirements/Installation

This software is written in PHP 7.X and uses a SQLite3 database. So it
needs a web server running to host it. I do this on my local computer
under Linux. I also host several other blogs on the Internet using this
software. I have not tested it on any other platform, but I have no
reason to believe there would be problems on another platform.

If you have a web server running, create a directory to host your blog,
and copy the Grotblog software there. Then go to that directory on your
web server, and run `index.php`, and you're there.

There are two users which are automatically installed on your system.
One is `anonymous`, which is sort of a catch-all and can't really do
anything. The other is `admin`. The password for admin is `password`.
Log in with that user and password, and do two things. First, change the
admin password. Second, create a new user for yourself, along with a
password.

That's it. There are no exotic requirements.

## Recovery

There is a possibility that some sociopath or sociopathic corporation will
remove/take down your blog. If this happens, you may not be able to recover
the content from where the site was hosted. There is a provision for this.
See the `HACKING.md` document for an explanation. On the "Admin" page,
there is a selection at the bottom of the page to "recover" your blog
contents if they're intact in the `articles/` directory.

## Warranty

I make no promises, neither explicit nor implied. This software works
for me and does what I have described above. It might not work for you.
If something goes wrong with it, you're invited to let me know so that I
can possibly fix the problem. Otherwise, you're on your own. If you've
never worked with HTML, CSS or markdown, I would encourage you to seek
some other, simpler blog software.


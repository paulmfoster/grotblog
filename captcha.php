<?php

// This file is needed to make captcha work.

// 2592000 = 30 days
ini_set('session.gc_maxlifetime', 2592000);
ini_set('session.cookie_lifetime', 2592000);
session_set_cookie_params(2592000);
session_start();

include 'libraries/fcaptcha.lib.php';
$c = new fcaptcha();

$image = $c->create_captcha($_SESSION['captcha']);
$c->render($image);

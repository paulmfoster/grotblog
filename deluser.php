<?php

include 'init.php';

access(0, DEFAULT_URL);

$userid = $_GET['id'] ?? NULL;

if (is_null($userid)) {
    redirect(ADMIN_URL);
}

$auser = $user->get_user($userid);	
if (!$auser) {
    // user is non-existent
    emsg('F', 'Cannot delete a non-existent user');
    redirect(ADMIN_URL);
}

$fields = array(
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Delete'
    ),
    'userid' => array(
        'name' => 'userid',
        'type' => 'hidden',
        'value' => $userid
    )
);

$form->set($fields);
$page_title = 'Delete User/Author';
$return = 'deluser2.php';

include VIEWDIR . 'deluser.view.php';


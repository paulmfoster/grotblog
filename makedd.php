<?php

// This is a convenient way to dump the table schema from SQLite.
// This script assumes all the tables have already been built, probably by
// hand. It then dumps them out to a PHP file which contains an array of
// SQL CREATE TABLE statements.

function instrument($label, $value)
{
    echo $label;
    echo '<pre>';
    print_r($value);
    echo '</pre>';
}

define('SYSDIR', 'system/');
define('LIBDIR', 'libraries/');
define('INCDIR', 'includes/');
define('CFGDIR', 'config/');
define('DATADIR', 'data/');

$cfg = parse_ini_file(CFGDIR . 'config.ini');

include INCDIR . 'errors.inc.php';
include LIBDIR . 'database.lib.php';
$db = new database($cfg['dsn']);

$sql = "SELECT sql FROM sqlite_master WHERE sql IS NOT NULL AND name != 'sqlite_sequence'";
$stmts = $db->query($sql)->fetch_all();
$max = count($stmts);

$str = '<?php' . PHP_EOL;
$str .= '$schema = [' . PHP_EOL;
$str .= "'PRAGMA foreign_keys=OFF'," . PHP_EOL;
$str .= "'BEGIN TRANSACTION'," . PHP_EOL;
for ($i = 0; $i < $max; $i++) {
    $str .= "\"{$stmts[$i]['sql']}\"";
    $str .= ',';
    $str .= PHP_EOL;
}
$str .= "'COMMIT'" . PHP_EOL;
$str .= '];' . PHP_EOL;

file_put_contents('coldstart.php', $str);


<?php

include 'init.php';

access(0, ADMIN_URL);

$login = $_POST['login'] ?? NULL;
if (is_null($login)) {
    redirect('adduser.php');
}

// password and confirm must match
if ($_POST['password'] !== $_POST['confirm']) {
    emsg('F', 'Password and confirmation do not match');
}

// error if login already exists
if ($user->add_user($_POST)) {
    emsg('S', 'User successfully added');
}

redirect(ADMIN_URL);


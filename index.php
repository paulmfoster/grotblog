<?php

include 'init.php';

$tsr = model('teasers', $db);

$teasers = $tsr->get('x');
if ($teasers == FALSE) {
    $view = 'no_articles.view.php';
}
else {
    $pager = $tsr->pager('X', NULL, 1, 'showtsr.php?page=');
    $view = 'teasers.view.php';
}

include VIEWDIR . $view;

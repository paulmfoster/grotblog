<?php

include 'init.php';

$period = $_POST['period'] ?? NULL;
if (is_null($period))
    redirect('index.php');

redirect("showtsr.php?period=$period");


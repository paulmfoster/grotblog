<?php

include 'init.php';

$fields = array(
    'login' => array(
        'name' => 'login',
        'type' => 'text',
        'size' => 12,
        'maxlength' => 30,
        'required' => 1,
        'class' => 'required'
    ),
    'password' => array(
        'name' => 'password',
        'type' => 'password',
        'required' => 1,
        'class' => 'required',
        'size' => 20,
        'maxlength' => 255
    ),
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Login'
    )
);

$form->set($fields);
$page_title = 'Log In';
$focus_field = 'login';
$return = 'login2.php';

include VIEWDIR . 'login.view.php';


<?php

class topics
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

	function add_topic($post)
	{
		if (empty($post['name'])) {
			emsg('F', 'Cannot add a topic with no name');
			return FALSE;
		}
		$name = filter_var($post['name'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_BACKTICK | FILTER_FLAG_ENCODE_HIGH);
		$abbrev = filter_var($post['abbrev'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_BACKTICK | FILTER_FLAG_ENCODE_HIGH);

		$sql = "SELECT id FROM topics WHERE name = '$name'";
		if ($this->db->query($sql)->fetch()) {
			emsg('F', 'Duplicate topic');
			return FALSE;
		}

		$this->db->insert('topics', array('abbrev' => $abbrev, 'name' => $name, 'qty' => 0));

		return TRUE;
	}

    function get_topic_from_id($id)
    {
		$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $sql = "SELECT * FROM topics WHERE id = $id";
        return $this->db->query($sql)->fetch();
    }

	function update_topic($post)
	{
		$name = filter_var($post['name'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_BACKTICK | FILTER_FLAG_ENCODE_HIGH);
		$this->db->update('topics', array('name' => $name), "id = {$post['topicid']}");

		return TRUE;
	}

	function delete_topic($id)
	{
		$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

		// id == 0 not a real topic ID
		if ($id == 0) {
			emsg('F', 'Unable to delete topic; no such topic');
			return FALSE;
		}

		// is this topic in use by one or more articles?
		$sql = "SELECT id FROM art2tops WHERE topicid = $id";
		if ($this->db->query($sql)->fetch_all()) {
			emsg('F', 'Unable to delete topic; topic in use');
			return FALSE;
		}

		// all okay; delete topic
		$sql = "DELETE FROM topics WHERE id = $id";
		$this->db->query($sql);
		return TRUE;
	}

}

<?php

// FIXME: is this a valid class? Seems incomplete, and some methods are
// duplicated elsewhere.

class blog
{
    public $db;

	function __construct($db)
	{
		$this->db = $db;
	}

	// GET METHODS ///////////////////////////////////////////

	function get_topic_names($artno)
	{
		$artno = filter_var($artno, FILTER_SANITIZE_NUMBER_INT);

		$sql = "SELECT t.name AS name FROM art2tops as p, topics as t WHERE p.artno = $artno AND t.id = p.topicid ORDER BY t.name";
		$names = $this->db->query($sql)->fetch_all();
		$max = count($names);
		$name = array();
		for ($i = 0; $i < $max; $i++) {
			$name[] = $names[$i]['name'];
		}
		$names_string = implode(', ', $name);

		return $names_string;
	}

	function get_article_topicids($artno)
	{
		$artno = filter_var($artno, FILTER_SANITIZE_NUMBER_INT);

		$sql = "SELECT topicid FROM art2tops WHERE artno = $artno";
		$t = $this->db->query($sql)->fetch_all();
		if ($t === FALSE) {
			return array();
		}
		$arr = array();
		$max = count($t);
		for ($i = 0; $i < $max; $i++) {
			$arr[] = $t[$i]['topicid'];
		}

		return $arr;
	}

	function get_article_topics($artno)
	{
		$artno = filter_var($artno, FILTER_SANITIZE_NUMBER_INT);

		$sql = "SELECT t.name AS name FROM art2tops as p, topics as t WHERE p.artno = $artno AND t.id = p.topicid ORDER BY t.name";
		$names = $this->db->query($sql)->fetch_all();
		if ($names === FALSE) {
			return FALSE;
		}
		$max_names = count($names);
		$name_array = array();
		for ($i = 0; $i < $max_names; $i++) {
			$name_array[] = $names[$i]['name'];
		}

		return $name_array;
	}

	function get_authors()
	{
		$sql = "SELECT * FROM user WHERE level = 0 ORDER BY name";
		$result = $this->db->query($sql)->fetch_all();

		return $result;
	}

	function topic($topicid)
	{
		$topicid = filter_var($topicid, FILTER_SANITIZE_NUMBER_INT);

		$sql = "SELECT * FROM topics WHERE id = $topicid";
		$result = $this->db->query($sql)->fetch();

		return $result;
	}

	function get_periods()
	{
		$sql = "SELECT * FROM periods ORDER BY period";
		$stats = $this->db->query($sql)->fetch_all();

		return $stats;
	}

	function get_total_articles()
	{
		return $this->total_articles;
	}


	function get_rss()
	{
		return $this->get_teasers();
	}

	function get_topics_string($artno)
	{
		$sql = "SELECT topicid, name FROM art2tops as p, topics as t WHERE t.id = p.topicid AND p.artno = $artno";
		$topics_recs = $this->db->query($sql)->fetch_all();
		if ($topics_recs !== FALSE) {
			$topics = array();
			foreach ($topics_recs as $tr) {
				$topics[] = $tr['name'];
			}
			$topics_str = implode(',', $topics);
		}
		else {
			$topics_str = '';
		}

		return $topics_str;
	}

	function get_topic_from_abbrev($abbrev)
	{
		$sql = "SELECT id, name FROM topics WHERE abbrev = '$abbrev'";
		$topic_rec = $this->db->query($sql)->fetch();
		if (!$topic_rec) {
			return FALSE;
		}
		return $topic_rec;
	}

	// ARTICLE METHODS //////////////////////////////////////////

	function blank_article()
	{
		// set up blank record
		$article = [
			'title' => '',
			'teaser' => '',
			'comments_allowed' => 0,
			'markup' => 'md',
			'topics' => array(),
			'topicnos' => array(),
			'topics_str' => '',
			'login' => '',
			'author' => '',
			'timestamp' => '',
			'display_date' => '',
			'content' => ''
		];

		return $article;
	}

	/**
	 * add_article()
	 *
	 * This assumes the user filled out the form, and all necessary
	 * fields were included (checked in the controller).
	 *
	 * @param array $post The POST array
	 * @param string $file The uploaded file
	 *
	 */

	function add_article($post, $files)
	{
		global $user;

		$article = $this->blank_article();

		$article['title'] = $post['title'];
		$article['teaser'] = $post['teaser'];
		$article['markup'] = $post['markup'];
		$article['comments_allowed'] = $post['comments_allowed'];

		$article['timestamp'] = str_replace('-', '', $post['timestamp']) . date('His');

		$u = $user->get_user_by_nonce($_SESSION['user']);
		$article['author'] = $u['name'];
		$article['authid'] = $u['id'];
		$article['login'] = $u['login'];

		foreach ($post['topics'] as $ptopic) {
			$btop = $this->get_topic($ptopic);
			$article['topics'][] = $btop['name'];
			$article['topicnos'][] = $btop['id'];
			$article['topicabbrevs'][] = $btop['abbrev'];
		}

		if (is_uploaded_file($files['upfile']['tmp_name'])) {
			move_uploaded_file($files['upfile']['tmp_name'], 'files/' . $files['upfile']['name']);
		}
		else {
			emsg('F', 'No file uploaded.');
			return FALSE;
		}

		$article['content'] = file_get_contents('files/' . $files['upfile']['name']);
		unlink('files/' . $files['upfile']['name']);

		if ($this->title_exists($article['title'])) {
			emsg('F', 'Title already exists');
			return FALSE;
		}

		$filename = $this->create_filename($article['title']);

		if (file_exists('articles/' . $filename . '.' . $article['markup'])) {
			emsg('F', 'Article file already exists');
			return FALSE;
		}	

		$this->db->begin();

		// insert into articles
		$rec = array(
			'author' => $article['authid'],
			'filename' => $filename . '.' . $article['markup'],
			'timestamp' => $article['timestamp'],
			'comments_allowed' => $article['comments_allowed'],
			'title' => $article['title']
		);
		$this->db->insert('articles', $rec);
		$artno = $this->db->lastid('articles');

		// update topics
		foreach ($article['topicnos'] as $topno) {
			$this->update_topics($topno, 1);
		}

		// update periods
		$period = substr($article['timestamp'], 0, 6);
		$this->update_periods($period, 1);

		// update teasers
		$this->db->insert('teasers', array('artno' => $artno, 'text' => $article['teaser'], 'markup' => $article['markup']));

		// update art2tops
		foreach ($article['topicnos'] as $topicno) {
			$this->db->insert('art2tops', array('artno' => $artno, 'topicid' => $topicno));
		}

		$this->db->commit();

		$ca = $article['comments_allowed'] ? '1' : '0';
		$topics_str = implode(',', $article['topicabbrevs']);

		// create metadata
		$metadata = [
			"title = \"${article['title']}\"",
			"login = \"${article['login']}\"",
			"markup = ${article['markup']}",
			"timestamp = \"${article['timestamp']}\"",
			"comments_allowed = $ca",
			"topics = \"$topics_str\"",
			"teaser = \"${article['teaser']}\""
		];

		file_put_contents('meta/' . $filename . '.ini', implode(PHP_EOL, $metadata));
		file_put_contents('articles/' . $filename . '.' . $article['markup'], $article['content']);
		$mail_meta = implode("\r\n", $metadata);

		$email = $u['email'];
		if (!empty($email)) {
			mail($email, $article['title'] . ' metadata', $mail_meta);
			emsg('S', 'Article successfully added. A copy will be emailed to you.');
		}
		else {
			emsg('S', 'Article successfully added.');
		}

		return TRUE;
	}

	// TOPIC METHODS ///////////////////////////////////////////////////


	// COMMENT METHODS ////////////////////////////////////////////////


	// POPULARITY METHODS /////////////////////////////////////////////

	function version()
	{
		return 5.0;
	}
};


<?php

class teasers
{
    public $db, $user;

    function __construct($db)
    {
        global $user;

        $this->db = $db;
        $this->user = $user;
    }

    // type: 'A' = author, 'T' = topic, 'P' = period, 'X' = all
    function get($type, $criterion = NULL, $page = 1)
    {
        global $cfg, $user;

        switch ($type) {
        case 'a':
            // author
            $author = filter_var($criterion, FILTER_SANITIZE_NUMBER_INT);
            $select = "SELECT * FROM articles WHERE author = $criterion";
            break;
        case 't':
            // topic
            $topic = filter_var($criterion, FILTER_SANITIZE_NUMBER_INT);
            $select = "SELECT articles.* FROM articles JOIN art2tops AS t ON (t.artno = articles.id) WHERE t.topicid = $criterion";
            break;
        case 'p':
            // period
            $period = filter_var($criterion, FILTER_SANITIZE_NUMBER_INT);
            $select = "SELECT * FROM articles WHERE timestamp LIKE '{$criterion}%'";
            break;
        default:
            // all
            $select = "SELECT * FROM articles";
            break;
        }

		$direction = ($cfg['sort_chrono'] == 0) ? 'DESC' : 'ASC';
        $order = "ORDER BY timestamp $direction";

        $limit = "LIMIT {$cfg['teasers_per_page']}";

        $ofs = ($page - 1) * $cfg['teasers_per_page'];
        $offset = "OFFSET $ofs";

        $sql = "$select $order $limit $offset";
        $teasers = $this->db->query($sql)->fetch_all();
        if ($teasers === FALSE) {
            return FALSE;
        }
        $max_teasers = count($teasers);

        // fetch users
        $users = $user->get_user_list();

        for ($j = 0; $j < $max_teasers; $j++) {
            // gather all the topic records for each article
            $sql = "SELECT name FROM topics WHERE id IN (SELECT topicid FROM art2tops WHERE artno = {$teasers[$j]['id']})";
            $topic_recs = $this->db->query($sql)->fetch_all();
            $topic_names = '';
            $max = count($topic_recs);
            for ($i = 0; $i < $max; $i++) {
                if ($i != 0) {
                    $topic_names = $topic_names . ',';
                }
                $topic_names = $topic_names . $topic_recs[$i]['name'];
            }
            $teasers[$j]['topic_names'] = $topic_names;

            // formulate x_timestamp
            $teasers[$j]['x_timestamp'] = explode_timestamp($teasers[$j]['timestamp']);

            // add author name
            foreach ($users as $u) {
                if ($teasers[$j]['author'] == $u['id']) {
                    $teasers[$j]['author_name'] = $u['name'];
                }
            }
        }

        return $teasers;
    }

    function get_rss()
        {
    }

    function pager($type, $criterion, $page, $url)
    {
        global $cfg;

        switch ($type) {
        case 'a':
            $author = filter_var($criterion, FILTER_SANITIZE_NUMBER_INT);
            $sql = "SELECT count(id) AS total FROM articles WHERE author = $author";
            break;
        case 't':
            $topic = filter_var($criterion, FILTER_SANITIZE_NUMBER_INT);
            $sql = "SELECT count(articles.id) AS total FROM articles JOIN art2tops AS t ON (t.artno = articles.id) WHERE t.topicid = $topic";
            break;
        case 'p':
            $period = filter_var($criterion, FILTER_SANITIZE_NUMBER_INT);
            $sql = "SELECT count(id) AS total FROM articles WHERE timestamp LIKE '{$period}%'";
            break;
        default:
            $sql = "SELECT count(id) AS total FROM articles";
            break;
        }

        $arts = $this->db->query($sql)->fetch();
        $total_items = $arts['total'];

        $pager = pager($total_items, $cfg['teasers_per_page'], $page, $cfg['pager_width'], $url);
        return $pager;

    }
}

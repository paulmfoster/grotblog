<?php

class admin
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    function get_topics_list()
	{
		$sql = "SELECT * FROM topics ORDER BY name";
		$results = $this->db->query($sql)->fetch_all();

		return $results;
    }

	/**
	 * get_articles()
	 *
	 * Used on the admin page to show a select list of articles.
	 *
	 * @return array $arts ID and title of articles
	 *
	 */

	function get_articles()
	{
		$sql = "SELECT id, title FROM articles ORDER BY title";
		$arts = $this->db->query($sql)->fetch_all();
		return $arts;
	}

	function get_article($artno)
	{
		global $user, $cfg;

		$artno = filter_var($artno, FILTER_SANITIZE_NUMBER_INT);

		// fetch the article data
		$sql = "SELECT * FROM articles WHERE id = $artno";
		$rec = $this->db->query($sql)->fetch();
		if ($rec === FALSE) {
			return FALSE;
		}

		$sql = "SELECT * FROM teasers WHERE artno = $artno";
		$tsr = $this->db->query($sql)->fetch();
		if ($rec === FALSE) {
			return FALSE;
		}

		$rec['topic_names'] = $this->get_topics_string($artno);

		// get username
		$user_rec = $user->get_user($rec['author']);
		$rec['user_name'] = $user_rec['name'];
		$rec['login'] = $user_rec['login'];


		$rec['period'] = substr($rec['timestamp'], 0, 6);
		$rec['pubdate'] = $this->explode_timestamp($rec['timestamp']);

		// get content
		
		$file_contents = file_get_contents('articles/' . $rec['filename']);
		if ($tsr['markup'] == 'md') {
			include $cfg['grottodir'] . 'Parsedown.php';
			$pd = new Parsedown();
			$rec['content'] = $pd->text($file_contents);
			// use line(), else we get paragraph tags before and after
			$rec['teaser'] = $pd->line($tsr['text']);
		}
		elseif ($tsr['markup'] == 'html') {
			$rec['content'] = $file_contents;
			$rec['teaser'] = $tsr['text'];
		}

		$this->log_usage($rec['title']);
		
		return $rec;
	}

	function get_topic($topicid)
	{
		$topicid = filter_var($topicid, FILTER_SANITIZE_NUMBER_INT);

		$sql = "SELECT * FROM topics WHERE id = $topicid";
		$topic = $this->db->query($sql)->fetch();
		return $topic;
	}

	function update_topic($post)
	{
		$name = filter_var($post['name'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_BACKTICK | FILTER_FLAG_ENCODE_HIGH);
		$this->db->update('topics', array('name' => $name), "id = {$post['topicid']}");

		return TRUE;
	}

	function delete_topic($id)
	{
		$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

		// id == 0 not a real topic ID
		if ($id == 0) {
			emsg('F', 'Unable to delete topic; no such topic');
			return FALSE;
		}

		// is this topic in use by one or more articles?
		$sql = "SELECT id FROM art2tops WHERE topicid = $id";
		if ($this->db->query($sql)->fetch_all()) {
			emsg('F', 'Unable to delete topic; topic in use');
			return FALSE;
		}

		// all okay; delete topic
		$sql = "DELETE FROM topics WHERE id = $id";
		$this->db->query($sql);
		return TRUE;
	}

	function get_max_popularity()
	{
		$sql = 'SELECT max(qty) AS max FROM articles';
		$rec = $this->db->query($sql)->fetch();
		if ($rec !== FALSE)
			return $rec['max'];
		else
			return 0;
	}
	
	function get_popularity()
	{
		$sql = "SELECT id as artno, title, qty FROM articles ORDER BY qty DESC";
		$pops = $this->db->query($sql)->fetch_all();
		return $pops;
	}
	

}

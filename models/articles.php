<?php

class articles
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

	function blank_article()
	{
		// set up blank record
		$article = [
			'title' => '',
			'teaser' => '',
			'markup' => 'md',
			'topics' => array(),
			'topicnos' => array(),
			'topics_str' => '',
			'login' => '',
			'author' => '',
			'timestamp' => '',
			'display_date' => '',
            'content' => '',
            'teaser' => '',
            'markup' => 'html',
            'qty' => 0
		];

		return $article;
	}

    function get_topic($id)
    {
        $sql = "SELECT * FROM topics WHERE id = $id";
        return $this->db->query($sql)->fetch();
    }

	function title_exists($title)
	{
		$quoted_title = database::quote($title);
		$sql = "SELECT id FROM articles WHERE title = $quoted_title";
		$result = $this->db->query($sql)->fetch();
		if ($result === FALSE) {
			return FALSE;
		}
		return TRUE;
	}

	// NOTE: this does not add the extension
	function create_filename($title)
	{
		// define "punctuation"
		$punct = array('~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '=', '{', '}', '|', '[', ']', '\\', ':', '"', ';', ',', '<', '>', '?', '\'', '.');
		// remove all punctuation
		$filename = str_replace($punct, '', $title);
		// convert slashes to spaces (errors otherwise)
		$filename = str_replace('/', ' ', $filename);
		// convert spaces to dashes (hyphens)
		$filename = str_replace(' ', '_', $filename);

		return $filename;
	}

	/**
	 * update_topics()
	 *
	 * @param integer $topic
	 * @param integer $qty
	 *
	 */

	function update_topics($topic, $qty)
	{
		$sql = "UPDATE topics SET qty = qty + $qty WHERE id = $topic";
		$this->db->query($sql);
	}

	/**
	 * update_periods()
	 *
	 * @param string $period What period to update?
	 * @param integer $qty Add or subtract this from periods
	 *
	 */

	function update_periods($period, $qty)
	{
		$sql = "SELECT qty FROM periods WHERE period = '$period'";
		$q = $this->db->query($sql)->fetch();
		if ($q !== FALSE) {
			$sql = "UPDATE periods SET qty = qty + $qty WHERE period = '$period'";
			$this->db->query($sql);
		}
		else {
			$this->db->insert('periods', array('period' => $period, 'qty' => $qty));
		}
	}

	/**
	 * add_article()
	 *
	 * This assumes the user filled out the form, and all necessary
	 * fields were included (checked in the controller).
	 *
	 * @param array $post The POST array
	 * @param string $file The uploaded file
	 *
	 */

	function add_article($post, $files)
	{
        global $user;

		$article = $this->blank_article();

		$article['title'] = $post['title'];
		$article['teaser'] = $post['teaser'];
		$article['markup'] = $post['markup'];

		$article['timestamp'] = str_replace('-', '', $post['timestamp']) . date('His');

		$u = $user->get_user_by_nonce($_SESSION['user']);
		$article['author'] = $u['name'];
		$article['authid'] = $u['id'];
		$article['login'] = $u['login'];

		foreach ($post['topics'] as $ptopic) {
			$btop = $this->get_topic($ptopic);
			$article['topics'][] = $btop['name'];
			$article['topicnos'][] = $btop['id'];
			$article['topicabbrevs'][] = $btop['abbrev'];
		}

		if (is_uploaded_file($files['upfile']['tmp_name'])) {
			move_uploaded_file($files['upfile']['tmp_name'], FILESDIR . $files['upfile']['name']);
		}
		else {
			emsg('F', 'No file uploaded.');
			return FALSE;
		}

		$article['content'] = file_get_contents(FILESDIR . $files['upfile']['name']);
		unlink(FILESDIR . $files['upfile']['name']);

		if ($this->title_exists($article['title'])) {
			emsg('F', 'Title already exists');
			return FALSE;
		}

		$filename = $this->create_filename($article['title']);

		if (file_exists(ARTSDIR . $filename . '.' . $article['markup'])) {
			emsg('F', 'Article file already exists');
			return FALSE;
		}	

		$this->db->begin();

		// insert into articles
		$rec = array(
			'author' => $article['authid'],
			'filename' => $filename . '.' . $article['markup'],
			'timestamp' => $article['timestamp'],
            'title' => $article['title'],
            'teaser' => $article['teaser'],
            'markup' => $article['markup'],
            'qty' => 0
		);
		$this->db->insert('articles', $rec);
		$artno = $this->db->lastid('articles');

		// update topics
		foreach ($article['topicnos'] as $topno) {
			$this->update_topics($topno, 1);
		}

		// update periods
		$period = substr($article['timestamp'], 0, 6);
		$this->update_periods($period, 1);

		// update art2tops
		foreach ($article['topicnos'] as $topicno) {
			$this->db->insert('art2tops', ['artno' => $artno, 'topicid' => $topicno]);
            // $sql = "INSERT INTO art2topics (artno, topicid) VALUES ($artno, $topicno)";
            // $this->db->query($sql);
		}

		$this->db->commit();

		$topics_str = implode(',', $article['topicabbrevs']);

		// create metadata
		$metadata = [
            "<!--",
			"title = \"${article['title']}\"",
			"login = \"${article['login']}\"",
			"markup = ${article['markup']}",
			"timestamp = \"${article['timestamp']}\"",
			"topics = \"$topics_str\"",
            "teaser = \"${article['teaser']}\"",
            "-->"
		];
        $metadata_str = implode(PHP_EOL, $metadata);

		file_put_contents(ARTSDIR . $filename . '.' . $article['markup'], $metadata_str . PHP_EOL . $article['content']);
		$mail_meta = implode("\r\n", $metadata);

		$email = $u['email'];
		if (!empty($email)) {
			mail($email, $article['title'] . ' metadata', $mail_meta);
			emsg('S', 'Article successfully added. A copy will be emailed to you.');
		}
		else {
			emsg('S', 'Article successfully added.');
		}

		return TRUE;
	}

	function delete_article($artno)
	{
		$artno = filter_var($artno, FILTER_SANITIZE_NUMBER_INT);

		// is this a real article? get metadata
		$sql = "SELECT * FROM articles WHERE id = $artno";
		$rec = $this->db->query($sql)->fetch();
		if (!$rec) {
			emsg('F', 'Cannot delete; no such article');
			return FALSE;
		}

		$period = substr($rec['timestamp'], 0, 6);
		$id = $rec['id'];

		$this->db->begin();

		$this->update_periods($period, -1);

		$sql = "SELECT * FROM art2tops WHERE artno = $id";
		$topics = $this->db->query($sql)->fetch_all();

		$this->db->delete('art2tops', "artno = $id");

		foreach ($topics as $topic) {
			$this->update_topics($topic['topicid'], -1);
		}

		$this->db->delete('articles', "id = $id");

		$this->db->commit();

		// delete the file
		$filename = $rec['filename'];
		unlink(ARTSDIR . $filename);

        // delete the metafile
        $base = basename($filename, '.' . $rec['markup']);
        $metafile = $base . '.ini';
        unlink(ARTSDIR . $metafile);

		emsg('S', 'Article deleted');
		return TRUE;

	}

    function get_topics_list()
	{
		$sql = "SELECT * FROM topics ORDER BY name";
		$results = $this->db->query($sql)->fetch_all();

		return $results;
    }

    private function get_topics_string($artno)
    {
        // gather all the topic records for each article
        $sql = "SELECT name FROM topics WHERE id IN (SELECT topicid FROM art2tops WHERE artno = $artno)";
        $topic_recs = $this->db->query($sql)->fetch_all();
        $topic_names = '';
        $max = count($topic_recs);
        for ($i = 0; $i < $max; $i++) {
            if ($i != 0) {
                $topic_names = $topic_names . ',';
            }
            $topic_names = $topic_names . $topic_recs[$i]['name'];
        }
        return $topic_names;
    }

	function get_article($artno)
	{
		global $user, $cfg;

		$artno = filter_var($artno, FILTER_SANITIZE_NUMBER_INT);

		// fetch the article data
		$sql = "SELECT * FROM articles WHERE id = $artno";
		$rec = $this->db->query($sql)->fetch();
		if ($rec === FALSE) {
			return FALSE;
		}

		$rec['topic_names'] = $this->get_topics_string($artno);

		// get username
		$user_rec = $user->get_user($rec['author']);
		$rec['user_name'] = $user_rec['name'];
		$rec['login'] = $user_rec['login'];


		$rec['period'] = substr($rec['timestamp'], 0, 6);
		$rec['pubdate'] = explode_timestamp($rec['timestamp']);

		// get content
		
		$file_contents = file_get_contents(ARTSDIR . $rec['filename']);
		if ($rec['markup'] == 'md') {
			include LIBDIR . 'Parsedown.php';
			$pd = new Parsedown();
			$rec['content'] = $pd->text($file_contents);
		}
		elseif ($rec['markup'] == 'html') {
			$rec['content'] = $file_contents;
		}

		$this->log_usage($rec['id']);
		
		return $rec;
	}

	function get_article_by_filename($filename)
	{
		global $user, $cfg;

		// fetch the article data
		$sql = "SELECT * FROM articles WHERE filename = '$filename'";
		$rec = $this->db->query($sql)->fetch();
		if ($rec === FALSE) {
			return FALSE;
		}

        $artno = $rec['id'];

		$rec['topic_names'] = $this->get_topics_string($artno);

		// get username
		$user_rec = $user->get_user($rec['author']);
		$rec['user_name'] = $user_rec['name'];
		$rec['login'] = $user_rec['login'];


		$rec['period'] = substr($rec['timestamp'], 0, 6);
		$rec['pubdate'] = explode_timestamp($rec['timestamp']);

		// get content
		
		$file_contents = file_get_contents(ARTSDIR . $rec['filename']);
		if ($rec['markup'] == 'md') {
			include LIBDIR . 'Parsedown.php';
			$pd = new Parsedown();
			$rec['content'] = $pd->text($file_contents);
		}
		elseif ($rec['markup'] == 'html') {
			$rec['content'] = $file_contents;
		}

		$this->log_usage($rec['id']);
		
		return $rec;
	}
	function get_comments($artno)
	{
		$artno = filter_var($artno, FILTER_SANITIZE_NUMBER_INT);
        $sql = "SELECT * FROM comments WHERE artno = $artno";
        $comments = $this->db->query($sql)->fetch_all();

		if ($comments !== FALSE) {
			$max_comments = count($comments);
			for ($i = 0; $i < $max_comments; $i++) {
				$comments[$i]['x_timestamp'] = explode_timestamp($comments[$i]['timestamp']);
			}
		}
		else {
			$comments = [];
		}
		return $comments;
	}

	function add_comment($post)
	{
		$artno = $post['artno'];

        $rec = [
            'artno' => $artno,
            'name' => $post['name'],
            'email' => $post['email'],
            'login' => $post['login'],
            'timestamp' => date('YmdHis'),
            'comment' => $post['comment']
        ];

        $this->db->insert('comments', $rec);
	}

	function delete_comment($comment_id)
	{
		$comment_id = filter_var($comment_id, FILTER_SANITIZE_NUMBER_INT);
		$sql = "DELETE FROM comments WHERE id = $comment_id";
		$this->db->query($sql);
	}

	/**
	 * log_usage()
	 *
	 * Increment the count of views for a particular article
	 *
	 * @param integer Article number
	 *
	 */

	function log_usage($artno)
	{
        $sql = "UPDATE articles SET qty = qty + 1 WHERE id = $artno";
        $this->db->query($sql);
	}

}

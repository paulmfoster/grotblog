<?php

// Show an article.

include 'init.php';

// $id = $_GET['id'] ?? NULL;
// if (is_null($id)) {
$filename = $_GET['filename'] ?? NULL;
if (is_null($filename))
    redirect(DEFAULT_URL);

// get article
$articles = model('articles', $db);
$article = $articles->get_article_by_filename($filename);

$page_title = '';

include VIEWDIR . 'showart.view.php';


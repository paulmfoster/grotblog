<?php

include 'init.php';


$login = $_POST['login'] ?? NULL;
if (is_null($login)) {
    redirect('register.php');
}

$result = $user->register($_POST);
if ($result === FALSE) {
    redirect(DEFAULT_URL);
}

$fields = array(
    'token' => array(
        'name' => 'token',
        'type' => 'text',
        'size' => 32,
        'required' => 1,
        'class' => 'required'
    ),
    's2' => array(
        'name' => 's2',
        'type' => 'submit',
        'value' => 'Send'
    )
);

$form->set($fields);
$page_total = 'Confirmation Needed';
$return = 'register3.php';
include VIEWDIR . 'register2.view.php';


<?php

include 'init.php';

$fields = array(
    'login' => array(
        'name' => 'login',
        'type' => 'text',
        'size' => 30,
        'maxlength' => 30,
        'required' => 1,
        'class' => 'required'
    ),
    'name' => array(
        'name' => 'name',
        'type' => 'text',
        'size' => 40,
        'maxlength' => 50,
        'required' => 1,
        'class' => 'required'
    ),
    'email' => array(
        'name' => 'email',
        'type' => 'text',
        'size' => 40,
        'maxlength' => 255,
        'required' => 1,
        'class' => 'required'
    ),
    'password' => array(
        'name' => 'password',
        'type' => 'password',
        'size' => 20,
        'maxlength' => 20,
        'required' => 1,
        'class' => 'required'
    ),
    'confirm' => array(
        'name' => 'confirm',
        'type' => 'password',
        'size' => 20,
        'maxlength' => 20,
        'required' => 1,
        'class' => 'required'
    ),
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Register'
    )
);

$form->set($fields);
$page_title = 'Register';
$return = 'register2.php';
include VIEWDIR . 'register.view.php';


# HACKING THIS SOFTWARE

## Input filtering

As of this version (2021-01-13) there is very little filtering of input. I
may add this later. But the fact is, this is your blog, and you're the one
doing the input. Most of the input is on the admin pages which you must
have permission to access anyway. If you want to screw your blog up by
entering goofy stuff to break your site, I'm not going to stop you. This
won't be adequate for the purists out there, and as I said, I may add input
filtering later. *Caveat emptor*. 

## Article storage

Grotblog does not store article content in its SQLite3 database. Articles
are stored as actual files in the `app/articles/` directory. Only metadata
is stored in the database. And even then, only enough for management
purposes. Metadata is also stored in a HTML comment in the top of your
article files, but only used in case of a recovery.

## Directories

There are several directories which are important.

1. `articles/` Where your articles go.
2. `config/` Your configuration file.
3. `data/` Your database lives here.
4. `files/` A temporary directory for your uploaded articles.
5. `images/` This mainly contains your header image for your blog.
6. `includes/`, `libraries/`, `models/` This is Grotblog code.
7. `printq/` For possible future use.
8. `views/` all your views go here.
9. The root directory of your site contains other Grotblog code.

## Model-View-Controller

There are several ways to implement the "Model-View-Controller" paradigm.
In the case of Grotblog, the models are in the `models/` directory, and the
views are in the `views/` directory. However, the controllers are not part
of a class in some "controllers" directory. Instead, each controller is a
"landing" page, and contains all the code related to that controller. There
is no "front controller". For years I wrote PHP code with front controllers
and controllers grouped into classes in a "controllers" directory. However,
I've found the code easier to understand and debug when page controllers
are their own files, as landing pages. It makes code and site navigation
simpler and easier.

## blog.sq3

This is the database which holds all the metadata for articles, topics,
and article-by-time-period. It's in the `app/data` directory.

## style.css

This is the CSS file for the blog. Out of the box, it's bland. Hack it
to your heart's content. It's in the `app/views` directory.

## colors.css

This file contains the colors for the major components of the website. They
should be self-explanatory. This file is included at the beginning of the
`style.css` file. It is also in the `app/views` directory.

## Right hand navigation

The links on the right side are set, and should not be changed. These
are called out in the head.view.php. This file is the template for most of
the things on the page.

## Left hand navigation

Out of the box, the following links are show on the left side of every
page:

* Home
* Contact
* About
* Policy

The first link sends the user back to the home page, `index.php`.

The second link is a contact form. Don't change this unless you know what
you're doing.

The third and fourth pages are what I'm calling "static pages". They are
not part of the blog articles; they're separate files. These are HTML
files contained in the `app/views/` directory. "About" is `about.html`.
"Policy" is `policy.html`. These files have contents in them already,
but the user may want to change them. See the INSTALL file for details.

It is possible to add to these "static" files, as detailed in the
INSTALL file. You must edit the `leftnav.php` file and add a link to
your file, similar to those of other static pages there. Then you load the
page into the `views/` directory. The `leftnav.php` file is just a PHP file
which defines a nested array of links.

## Modifyable Files

The following files can be or will be modified in the course of running
Grotblog:

- `views/colors.css` You may change the colors for your site here.
- `views/style.css` You may change aspects of the site which are controlled
    by CSS.
- `leftnav.php` You may add "static" pages here.
- `config/config.ini` Here you may change the name of the Blog, its slogan,
    etc. This is editable via the Admin page.
- `data/blog.sq3` This is your database. It comes with an admin user. You may
    add users. It also gets modified when you add articles.
- `images/header-background.jpg` The header image for your blog.
- `views/about.html` This is a generic page as supplied. Edit it to your
    needs.
- `views/policy.html` This is a generic "rules" page which you shouldn't need
    to change, but you can if you like.

## Recovery

In this age of censorship, it's possible that your blog could be removed
without your permission. If you still have a copy of the software and your
content, then you can recover your blog to almost new condition.

1. Find a new place to host your blog.
2. Install the Grotblog software there.
3. FTP (upload) your article files from the old blog to the `articles/`
   directory.
4. Log in using the `admin` user and `password` password.
5. Go to the admin screen, and push the "Recover" button at the bottom.

After recovery, all your articles should be visible, but you will want to
make some other changes.

1. In the Admin page, go to each user and select "Edit" to edit them.
    - Change the name to the actual name of the user (not its username or abbreviation). 
    - Change the email address to one that works (not `me@example.com`). 
    - Change the password to something more secure.
2. In the Admin page, change the category names. After recovery, the
   category names will be the same as their abbreviations, Change the
   names to something more readable.
3. Optionally, if you've added "static" pages (showing on the left), these
   must be put back into the `articles/` directory if not already there.
   You may also need to edit the `leftnav.php` file to make these visible.

Considering the above, I strongly suggest you back up your content files
before uploading them. The recovery script is nice to have, but nothing
beats having your own backup of your site and its content and metafiles.



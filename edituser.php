<?php

include 'init.php';

access(0, DEFAULT_URL);

$userid = $_GET['id'] ?? NULL;

if (is_null($userid)) {
    redirect(ADMIN_URL);
}

$auser = $user->get_user($userid);	
if (!$auser) {
    // user is non-existent
    emsg('F', 'Cannot edit a non-existent user');
    redirect(ADMIN_URL);
}

$login = $auser['login'];

$level_options = array(
    array('lbl' => 'Admin', 'val' => 0),
    array('lbl' => 'Other', 'val' => 255)
);

$fields = array(
    'id' => array(
        'name' => 'id',
        'type' => 'hidden',
        'value' => $userid
    ),
    'name' => array(
        'name' => 'name',
        'type' => 'text',
        'size' => 30,
        'maxlength' => 50,
        'value' => $auser['name']
    ),
    'email' => array(
        'name' => 'email',
        'type' => 'text',
        'size' => 30,
        'maxlength' => 255,
        'value' => $auser['email']
    ),
    'password' => array(
        'name' => 'password',
        'type' => 'password',
        'size' => 30,
        'maxlength' => 255
    ),
    'confirm' => array(
        'name' => 'confirm',
        'type' => 'password',
        'size' => 30,
        'maxlength' => 255
    ),
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Update'
    )
);

$form->set($fields);
$page_total = 'Edit User/Author';
$focus_field = 'name';
$return = 'edituser2.php';
include VIEWDIR . 'edituser.view.php';



<?php

include 'init.php';

$articles = model('articles', $db);

$markup_options = [
    ['lbl' => 'Markdown', 'val' => 'md'],
    ['lbl' => 'HTML', 'val' => 'html']
];

$topics = $articles->get_topics_list();
// This field is a multiple select. In a single select field, the top
// option is automatically selected. Not so in a multiple select. So, in
// case the user tries to breeze on by, we automatically select the
// first item, using this variable.
$selected = $topics[0]['id'];
$topic_options = [];
foreach ($topics as $topic) {
    $topic_options[] = ['lbl' => $topic['name'], 'val' => $topic['id']];
}

$fields = [
    'MAX_FILE_SIZE' => [
        'name' => 'MAX_FILE_SIZE',
        'type' => 'hidden'
    ],
    'upfile' => [
        'name' => 'upfile',
        'type' => 'file',
        'required' => 1,
        'class' => 'required'
    ],
    'title' => [
        'name' => 'title',
        'type' => 'text',
        'size' => 50,
        'maxlength' => 50,
        'required' => 1,
        'class' => 'required'
    ],
    'markup' => [
        'name' => 'markup',
        'type' => 'select',
        'required' => 1,
        'options' => $markup_options
    ],
    'timestamp' => [
        'name' => 'timestamp',
        'type' => 'date',
        'required' => 1,
        'class' => 'required'
    ],
    'topics' => [
        'name' => 'topics',
        'type' => 'select',
        'required' => 1,
        'multi' => 1,
        'options' => $topic_options
    ],
    'teaser' => [
        'name' => 'teaser',
        'type' => 'text',
        'size' => 50,
        'maxlength' => 255,
        'required' => 1,
        'class' => 'required'
    ],
    's1' => [
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Save'
    ]
];

$form->set($fields);
$page_title = 'Add Article';
$return = 'addart2.php';
$focus_field = 'title';

include VIEWDIR . 'addart.view.php';



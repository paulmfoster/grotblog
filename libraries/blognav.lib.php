<?php

class blognav
{
    public $db;
    public $user;

	function __construct($db, $user)
	{
		$this->db = $db;
        $this->user = $user;
	}

	/**
	 * nav()
	 *
	 * Creates a HTML string with is a hierarchical list of links with
	 * headings.
	 *
	 * Links are in the form of:
	 *
	 * 'Fruits' => [
	 * 		'Grapes' => [
	 * 			'Red Grapes' => 'redgrapes.com',
	 * 			'White Grapes' => 'whitegrapes.com'
	 * 		],
	 * 		'Peaches' => 'peaches.com'
	 *	];
	 *
	 * @param array $link_array Hierarchical array of links
	 *
	 * @return string The HTML of the array of links
	 */
	
	function show($link_array, $direction)
	{
		if ($direction == 'L') {
			$nhead = 'lnavhead';
			$nlink = 'lnavlink';
		}
		else {
			$nhead = 'rnavhead';
			$nlink = 'rnavlink';
		}

		$str = '';
		foreach ($link_array as $title => $links) {
			$str .= '<div class="' . $nhead . '">' . $title . '</div>' . PHP_EOL;
			$str .= '<ul>' . PHP_EOL;
			foreach ($links as $link => $url) {
				$str .= '<li class="' . $nlink . '"><a href="' . $url . '">' . $link . '</a></li>' . PHP_EOL;
			}
			$str .= '</ul>' . PHP_EOL;
		}

		return $str;
	}

	/**
	 * left_nav()
	 *
	 * Called in the head file, this returns a string which defines the
	 * list of links for left navigation.
	 *
	 * Usage: echo $nav->left_nav();
	 *
	 * @return string The HTML for a list of links
	 */

	function left_nav()
	{
		// define the $links variable
		include 'leftnav.php';
		$str = '<div id="nav-left">' . PHP_EOL;
		$str .= $this->show($links, 'L');
		$str .= '</div> <!-- nav-left -->' . PHP_EOL;

		return $str;
	}

	function get_topics_list()
	{
		$sql = "SELECT * FROM topics ORDER BY name";
		$results = $this->db->query($sql)->fetch_all();

		return $results;
	}

	function get_topics_sidebar()
	{
		if (!$topics = $this->get_topics_list()) {
			return array();
		}
		$x_topics = array();
		$max_topics = count($topics);
		for ($x = 0; $x < $max_topics; $x++) {
			// $x_topics[$topics[$x]['name'] . ' (' . $topics[$x]['qty'] . ')'] = 'index.php?url=tsr/show/t/' . $topics[$x]['id'] . '/1';
			$x_topics[$topics[$x]['name'] . ' (' . $topics[$x]['qty'] . ')'] = 'showtsr.php?topic=' . $topics[$x]['id'] . '&page=1';
		}
		return $x_topics;
	}

	function get_periods_sidebar()
	{
		$month_names = array(
			'01' => 'January',
			'02' => 'February',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December');

		$sql = "SELECT * FROM periods ORDER BY period";
		if (!$stats = $this->db->query($sql)->fetch_all()) {
			return array();
		}

		$x_stats = array();
		$max_stats = count($stats);
		for ($x = 0; $x < $max_stats; $x++) {
			$year = substr($stats[$x]['period'], 0, 4);
			$month_num = substr($stats[$x]['period'], 4, 2);
			$month_name = $month_names[$month_num];
			$legend = $month_name . ' ' . $year . ' (' . $stats[$x]['qty'] . ')';
			$x_stats[$legend] = $stats[$x]['period'];
		}
		return $x_stats;
	}

	function get_authors_sidebar()
	{
        $users = $this->user->get_admin_users();
		$authors = array();
		foreach ($users as $user) {
			$sql = "SELECT count(id) AS total FROM articles WHERE author = {$user['id']}";
			$total = $this->db->query($sql)->fetch();
			if ($total['total'] != 0) {
				// $authors[$user['name'] . ' (' . $total['total'] . ')'] = 'index.php?url=tsr/show/a/' . $user['id'] . '/1';
				$authors[$user['name'] . ' (' . $total['total'] . ')'] = 'showtsr.php?author=' . $user['id'] . '&page=1';
			}
		}

		return $authors;

	}

	/**
	 * right_nav()
	 *
	 * Called in the head file, this returns a string which defines the
	 * list of links for right navigation.
	 *
	 * Usage: echo $nav->right_nav();
	 *
	 * @return string The HTML for a list of links
	 */

	function right_nav()
	{
		if (isset($_SESSION['user'])) {
			$links = array(
				'Administration' => array(
					'Logout' => 'logout.php',
					'Admin' => 'admin.php'
				),
				'Topics' => $this->get_topics_sidebar(),
				'Authors' => $this->get_authors_sidebar()
			);
		}
		else {
			$links = array(
				'Administration' => array(
					'Login' => 'login.php',
					'Register' => 'register.php',
				),
				'Topics' => $this->get_topics_sidebar(),
				'Authors' => $this->get_authors_sidebar()
			);
		}

		$plinks = $this->get_periods_sidebar();

		$str = '<div id="nav-right">' . PHP_EOL;
		$str .= $this->show($links, 'R');

		$str .= '<div class="rnavhead">Archives</div>' . PHP_EOL;
		$str .= '<form method="post" action="period_form.php"/>' . PHP_EOL;
		$str .= '<select name="period">' . PHP_EOL;
		foreach ($plinks as $label => $link) {
			$str .= '<option value="' . $link . '">' . $label . '</option>' . PHP_EOL;
		}
		$str .= '</select>' . PHP_EOL;
		$str .= '<input type="submit" name="archive_submit" id="archive_submit" value="Select"/>' . PHP_EOL;
		$str .= '</form>' . PHP_EOL;

		$str .= '</div> <!-- nav-right -->' . PHP_EOL;

		return $str;
	}

}



<?php

/**
 * Class to implement graphic captcha
 *
 * This class uses the FreeMonoBold font, but can be changed.
 *
 * Implementation:
 *
 * Include the file "arial.ttf" in the directory, or other TTF file of
 * your choice (change code as needed).
 *
 * At the top of the page:
 * ----------------------
 *
 * session_start();
 *
 * require_once 'fcaptcha.lib.php';
 * $captcha = new captcha;
 *
 * check for form contents
 * if ($captcha->validate($_POST['ruhuman'], 'captcha') {
 * 		// take action
 * }
 *
 * $code = $captcha->get_captcha(6);
 * $captcha->set_session('captcha', $code);
 *
 * In the form:
 * -----------
 *
 * <img src="captcha.php"/>
 * add form field for reply
 *
 * In a file called "captcha.php":
 * ------------------------------
 *
 * session_start();
 * include 'captcha.lib.php';
 * $c = new captcha();
 * $image = $c->create_captcha($_SESSION['captcha']);
 * $c->render($image);
 *
 */

class fcaptcha
{
	function get_captcha($len)
	{
		$random_alpha = md5(random_bytes(64));
		$captcha_code = substr($random_alpha, 0, $len);
		return $captcha_code;
	}

	function set_session($key, $value)
	{
		$_SESSION[$key] = $value;
	}

	function get_session($key)
	{
		if (!empty($key) && isset($_SESSION[$key]) && !empty($_SESSION[$key])) {
			return $_SESSION[$key];
		}
		else {
			return '';
		}
	}

	function create_captcha($str)
	{
		$img = imagecreatetruecolor(90, 30); // 100, 40
		$bg = imagecolorallocate($img, 211, 211, 211); // gray
		$fg = imagecolorallocate($img, 0, 0, 0); // black
		$red = imagecolorallocate($img, 255, 0, 0); // red

		imagefill($img, 0, 0, $bg);
		// $font = './arial.ttf';
		$font = './FreeMonoBold.ttf';
		// params: img, size in pts, angle in degrees, x, y, color, fontfile, text
		imagettftext($img, 16, 0, 10, 22, $red, $font, $str);
		// imagestring($img, 5, 0, 0, $str, $fg);

		return $img;
	}

	function render($img)
	{
		header('Cache-Control: no-cache, must-revalidate');
		header('Content-type: image/jpeg');

		imagejpeg($img);
	}

	function validate($post_data, $key)
	{
		$session_data = $this->get_session($key);
		if ($session_data === $post_data) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}

}


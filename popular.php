<?php

include 'init.php';

access(0, ADMIN_URL);

$admin = model('admin', $db);

$pops = $admin->get_popularity();
$max = $admin->get_max_popularity();
$page_title = 'Article Popularity';
include VIEWDIR . 'popular.view.php';


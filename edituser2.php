<?php

include 'init.php';

$userid = $_POST['id'] ?? NULL;
if (is_null($userid)) {
    redirect(ADMIN_URL);
}

if ($user->update_user($_POST)) {
    emsg('S', 'User successfully updated');
}

redirect(ADMIN_URL);


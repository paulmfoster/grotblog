<?php

include 'init.php';

$topics = model('topics', $db);

access(0, DEFAULT_URL);

$topicid = $_POST['topicid'] ?? NULL;
if (is_null($topicid)) {
    redirect(ADMIN_URL);
}

$topics->update_topic($_POST);
emsg('S', 'Topic edits saved');
redirect(ADMIN_URL);


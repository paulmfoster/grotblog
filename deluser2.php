<?php

include 'init.php';

$userid = $_POST['userid'] ?? NULL;
if (is_null($userid)) {
    redirect(ADMIN_URL);
}

if ($user->delete_user($userid)) {
    emsg('S', 'User successfully deleted');
}

redirect(ADMIN_URL);


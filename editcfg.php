<?php

include 'init.php';

access(0, DEFAULT_URL);

$ncfg = file_get_contents(CFGDIR . 'config.ini');

$fields = [
    'ncfg' => [
        'name' => 'ncfg',
        'type' => 'textarea',
        'rows' => 50,
        'cols' => 65,
        'value' => $ncfg
    ],
    's1' => [
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Save'
    ]
];
$form->set($fields);
$return = 'editcfg2.php';
include VIEWDIR . 'editcfg.view.php';

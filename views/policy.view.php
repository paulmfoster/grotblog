
<p>

This blog is a forum for my ideas.

</p>
<p>

I welcome your comments. And I expect them to be
<strong>civil</strong>. When I believe they are not, I reserve the
right to remove them and/or block your ability to comment temporarily or
permanently. I exercise these options is at my discretion, but it has
little to do with whether you disagree with me or not.  It has more to
do with whether your arguments are silly or stupid, and/or whether you
are excessively harsh and rude. Remember the word
<strong>civil</strong>.

</p>

<h2>Privacy Policy</h2>

<p>

You are welcome to comment on this blog anonymously.
If you register with
my site, I ask for your actual name and email address. The name you
provide to us will be associated with any comments you make while logged
in. The email address is not made public in any way, but may be used by
me for private email conversations with you, if such seem appropriate.
<strong>We do not sell or otherwise make public email
addresses</strong>.

</p>
<p>

People say all manner of things to each other privately which would be
horribly inappropriate if said publicly.  Please be aware that anything
you post on this site is automatically made public, including your name
if you are logged in while commenting. That's the nature of a forum such
as this. Please consider this before posting.

</p>
<p>

Other than obviously public information as detailed above, I share none
of your private information with others.  The only exceptions are:

<ol>

<li>If I am asked to reveal such information by legal authorities.</li>

<li>I am reasonably certain, from public or private communications
with me, that you are actively committing or have committed crimes for
which you have eluded prosecution. In this case, I may volunteer this
information to law enforcement or legal authorities.</li>

<li>You attempt to hack this site. In this case, I will enlist the
assistance of security professionals and do what I can to track you
down and prosecute you to the fullest extent possible.  </li>

</ol>

</p>
<p>

I purposely don't collect a lot of private information about people who
surf to our site or comment. I'm not in the business of email
marketing or selling private information, and frankly I don't want to
be liable for knowing any more than I have to about you. Should you
engage me in private conversations, notwithstanding the three areas
above, I will not reveal any private information you may give me,
unless you give me express permission to do so. Should you say something
so pithy in private that I'd like to make it part of my posts, I'll
ask you first, and won't make it public unless you expressly agree.

</p>

<h2>Copyright</h2>

<p>

All blog posts are copyright by their author(s). The
software which operates this site is &copy; <a
href="http://noferblatz.com">Paul M. Foster</a>
under the <a href="http://www.gnu.org/licenses/gpl-2.0.html"> GNU Public
License Version 2</a>.  You own copyrights to your comments, with the
proviso that I reserve the right to display them as part of this blog,
in perpetuity. Any other copying or use of your comments is at your
discretion. Please be aware that I am incapable of preventing
<em>other people</em> from quoting or using your comments elsewhere.

</p>



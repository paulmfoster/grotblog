<?php include VIEWDIR . 'head.view.php'; ?>
<h2><span class="red">Are you SURE you want to delete this article?</span></h2>

<form method="post" action="<?php echo $return; ?>">
<?php $form->hidden('artno'); ?>
<?php $form->submit('s1'); ?>
</form>

<h2><?php echo $article['title']; ?> (<?php echo $article['topic_names']; ?>)</h2>
<div class="article_meta">
<?php echo $article['user_name']; ?> (<?php echo $article['pubdate']; ?>)
</div>

<!-- article content -->
<span class="article"><?php echo $article['content']; ?></span>
<!-- end article content -->
<?php include VIEWDIR . 'footer.view.php'; ?>

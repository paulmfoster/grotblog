<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $cfg['charset']; ?>" />
<link rel="shortcut icon" href="<?php echo VIEWDIR . 'favicon.ico'; ?>">
<link href="<?php echo VIEWDIR . 'style.css?v=' . date('His'); ?>" rel="stylesheet" type="text/css" />

<title><?php echo $cfg['blog_title']; ?></title>

</head>

<?php if (isset($focus_field)): ?>
	<body onLoad="document.getElementById('<?php echo $focus_field; ?>').focus();">
<?php else: ?>
	<body>
<?php endif; ?>

<a name="top" id="top"></a>

<div class="container">

	<div id="header">
		<div class="header-text">
			<span class="header-title"><?php echo $cfg['blog_title']; ?></span>
			<br/>
			<span class="header-subtitle"><?php echo $cfg['blog_subtitle']; ?></span>
		</div>
	</div> <!-- header -->

<!-- left_side -->
<?php echo $nav->left_nav(); ?>
<!-- right side -->
<?php echo $nav->right_nav(); ?>

<!-- content -->
<div id="content">

<!-- MESSAGES ------------------------------>
<?php show_messages(); ?>
<!-- END OF MESSAGES ---------------------->

<?php if (isset($page_title)): ?>
<?php echo '<div class="page-title">' . $page_title . '</div>'; ?>
<?php endif; ?>


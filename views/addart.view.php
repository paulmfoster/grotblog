<?php include VIEWDIR . 'head.view.php'; ?>
<p>
<form method="post" action="<?php echo $return; ?>" enctype="multipart/form-data">

<table>

<tr>
<td>
<label>Title</label><br/>
<?php $form->text('title'); ?>
</td>
</tr>

<tr>
<td>
<label>Teaser</label><br/>
<?php $form->text('teaser'); ?>
</td>
</tr>

<tr>
<td>
<label>Topics</label><br/>
<?php $form->select('topics', $selected); ?>
</td>
</tr>

<tr>
<td>
<label>Markup Type</label><br/>
<?php $form->select('markup'); ?>
</td>
</tr>

<tr>
<td>
<label>Timestamp</label><br/>
<?php $form->date('timestamp', date('Y-m-d')); ?>
</td>
</tr>

<tr>
<td>
<label for="upfile">Upload</label>
<?php $form->hidden('MAX_FILE_SIZE', 1000000); ?>
<?php $form->file('upfile'); ?>
</td>
</tr>

</table>

<p>
<?php $form->submit('s1'); ?>
</p>
</form>

<?php include VIEWDIR . 'footer.view.php'; ?>


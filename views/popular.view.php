<?php include VIEWDIR . 'head.view.php'; ?>
<br/>
<?php if ($pops === FALSE): ?>
<h2>No popularity scores available</h2>
<?php else: ?>
<table>
<tr><th>Title</th><th>Hits</th></tr>
<?php foreach ($pops as $pop): ?>
	<tr>
	<td><?php echo $pop['title']; ?></td>
	<td><?php echo $pop['qty']; ?></td>
	</tr>
<?php endforeach; ?>	
</table>
<?php endif; ?>
<?php include VIEWDIR . 'footer.view.php'; ?>

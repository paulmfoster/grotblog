<?php include VIEWDIR . 'head.view.php'; ?>

<!-- teasers -->
<br/>
<?php foreach ($teasers as $teaser): ?>
	<span class="article-title"><?php echo $teaser['title']; ?></span>
	<br/>
	<span class="article-topics">(<?php echo $teaser['topic_names']; ?>)</span>
	<div class="article-meta">
	by <?php echo $teaser['author_name']; ?> (<?php echo $teaser['x_timestamp']; ?>)
	</div>
	<p>
	<span class="article"><?php echo $teaser['teaser']; ?></span>
	<br/>
    <a href="<?php echo 'showart.php?filename=' . $teaser['filename']; ?>">Read&nbsp;More</a></p>
	<hr/>
<?php endforeach; ?>
<!-- end teasers -->
<?php if (!empty($pager)): ?>
<!-- pager -->
<?php echo $pager; ?>
<!-- end pager -->
<?php endif; ?>

<?php include VIEWDIR . 'footer.view.php'; ?>


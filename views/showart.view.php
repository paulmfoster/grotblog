<?php include VIEWDIR . 'head.view.php'; ?>

<br/>
	<span class="article-title"><?php echo $article['title']; ?></span>
	<br/>
	<span class="article-topics">(<?php echo $article['topic_names']; ?>)</span>
	<div class="article-meta">
	<?php echo $article['user_name']; ?> (<?php echo $article['pubdate']; ?>)
	</div>

<!-- article content -->
<span class="article"><?php echo $article['content']; ?></span>
<!-- end article content -->

<?php include VIEWDIR . 'footer.view.php';


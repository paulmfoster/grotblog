<?php include VIEWDIR . 'head.view.php'; ?>
<p>
Fields marked with yellow background are mandatory.
</p>

<form method="post" action="<?php echo $return; ?>">
<table>

<tr>
<td>
<label for="login">Login</label>
</td>
</tr>
<tr>
<td>
<?php $form->text('login'); ?>
</td>
</tr>

<tr>
<td>
<label for="name">Name</label>
</td>
</tr>
<tr>
<td>
<?php $form->text('name'); ?>
</td>
</tr>

<tr>
<td>
<label for="email">Email</label>
</td>
</tr>
<tr>
<td>
<?php $form->text('email'); ?>
</td>
</tr>

<tr>
<td>
<label for="password">Password</label>
</td>
</tr>
<tr>
<td>
<?php $form->password('password'); ?>
</td>
</tr>

<tr>
<td>
<label for="confirm">Confirm Password</label>
</td>
</tr>

<tr>
<td>
<?php $form->password('confirm'); ?>
</td>
</tr>

<tr>
<td>
<?php $form->submit('s1'); ?>
</td>
</tr>

</table>

</form>
<?php include VIEWDIR . 'footer.view.php'; ?>


<?php include VIEWDIR . 'head.view.php'; ?>
<p>
Shortly, you should receive an email at the email address
you gave us. This email will contain a "token" which will
allow you to finish your registration. Simply copy and
paste the token in the blank below.
</p>

<form action="<?php echo $return; ?>" method="post">

<label for="token">Token</label>
<br/>
<?php $form->text('token'); ?>
<br/>
<?php $form->submit('s2'); ?>

</form>
<?php include VIEWDIR . 'footer.view.php'; ?>

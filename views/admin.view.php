<?php include VIEWDIR . 'head.view.php'; ?>
<p>

<form action="<?php echo $return; ?>" method="post">

<fieldset>
    <legend><strong>Articles</strong></legend>
	<?php form::button('Add Article', 'addart.php'); ?><br/>
	<hr>
	For deleting, select an article:<br/>
<?php $form->select('artno'); ?>
<br/>
<?php $form->submit('artdel'); ?>

</fieldset>

<fieldset>
    <legend><strong>Topics</strong></legend>
	<?php $form->button('Add Topic', 'addtop.php'); ?>
	<hr>
For editing/deleting, select a topic:<br/>
<?php $form->select('topicid'); ?>
<br/>
<?php $form->submit('topedt'); ?>
&nbsp;
<?php $form->submit('topdel'); ?>
</fieldset>

<fieldset>
    <legend><strong>Users</strong></legend>
<?php form::button('Add User', 'adduser.php'); ?>
	<hr>
For editing/deleting, select a user:<br/>
<?php $form->select('userid'); ?>
<br/>
<?php $form->submit('usredt'); ?>
&nbsp;
<?php $form->submit('usrdel'); ?>

</fieldset>

<fieldset>
    <legend><strong>Configuration</strong></legend>
    <table>
        <tr>
			<td>
			<?php $form->submit('cfgedt'); ?>
			</td>
        </tr>
    </table>
</fieldset>
</form>

<fieldset>
	<legend><strong>Popularity</strong></legend>
	<table>
		<tr>
			<td>
			<?php form::button('Check popularity', 'popular.php'); ?>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend><strong>Recover</strong></legend>
	<table>
		<tr>
			<td>
			<?php form::button('Recover Blog', 'recover.php'); ?>
			</td>
		</tr>
	</table>
</fieldset>


<?php include VIEWDIR . 'footer.view.php'; ?>


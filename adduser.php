<?php

include 'init.php';

access(0, DEFAULT_URL);

$level_options = array(
    array('lbl' => 'Admin', 'val' => 0),
    array('lbl' => 'Other', 'val' => 255)
);

$fields = array(
    'login' => array(
        'name' => 'login',
        'type' => 'text',
        'size' => 30,
        'maxlength' => 30,
        'required' => 1,
        'class' => 'required'
    ),
    'name' => array(
        'name' => 'name',
        'type' => 'text',
        'size' => 30,
        'maxlength' => 50,
        'required' => 1,
        'class' => 'required'
    ),
    'email' => array(
        'name' => 'email',
        'type' => 'text',
        'size' => 30,
        'maxlength' => 255,
        'required' => 1,
        'class' => 'required'
    ),
    'level' => array(
        'name' => 'level',
        'type' => 'radio',
        'direction' => 'R',
        'required' => 1,
        'class' => 'required',
        'options' => $level_options
    ),
    'password' => array(
        'name' => 'password',
        'type' => 'password',
        'size' => 30,
        'maxlength' => 255,
        'required' => 1,
        'class' => 'required'
    ),
    'confirm' => array(
        'name' => 'confirm',
        'type' => 'password',
        'size' => 30,
        'maxlength' => 255,
        'required' => 1,
        'class' => 'required'
    ),
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Add'
    )
);

$form->set($fields);
$page_total = 'Add Author';
$return = 'adduser2.php';
include VIEWDIR . 'adduser.view.php';
$this->view('authadd.view.php');

<?php

// the main admin screen

include 'init.php';

$admin = model('admin', $db);

access(0, DEFAULT_URL);

$topics = $admin->get_topics_list();
$users = $user->get_user_list();
$articles = $admin->get_articles();

$artno_options = array();
if ($articles !== FALSE) {
    foreach ($articles as $article) {
        $artno_options[] = array('lbl' => $article['title'], 'val' => $article['id']);
    }
}
else
    $artno_options = array();

$topics_options = [];
if ($topics)
    foreach ($topics as $topic) {
        $topics_options[] = array('lbl' => $topic['name'], 'val' => $topic['id']);
    }
else
    $topics_options = array();

$user_options = array();
foreach ($users as $auser) {
    $user_options[] = array('lbl' => $auser['name'], 'val' => $auser['id']);
}

$fields = array(
    'artno' => array(
        'name' => 'artno',
        'type' => 'select',
        'options' => $artno_options),
    'artdel' => array(
        'name' => 'artdel',
        'type' => 'submit',
        'value' => 'Delete Article'),
    'topicid' => array(
        'name' => 'topicid',
        'type' => 'select',
        'options' => $topics_options),
    'topedt' => array(
        'name' => 'topedt',
        'type' => 'submit',
        'value' => 'Edit Topic'),
    'topdel' => array(
        'name' => 'topdel',
        'type' => 'submit',
        'value' => 'Delete Topic'),
    'userid' => array(
        'name' => 'userid',
        'type' => 'select',
        'options' => $user_options),
    'usredt' => array(
        'name' => 'usredt',
        'type' => 'submit',
        'value' => 'Edit User'),
    'usrdel' => array(
        'name' => 'usrdel',
        'type' => 'submit',
        'value' => 'Delete User'),
    'cfgedt' => array(
        'name' => 'cfgedt',
        'type' => 'submit',
        'value' => 'Edit Configuration')
);

$form->set($fields);
$page_title = 'Administration';
$return = 'admin2.php';

include VIEWDIR . 'admin.view.php';


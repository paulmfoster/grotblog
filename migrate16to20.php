<?php

// This script transitions from the 1.6 to 2.0 versions of grotblog.
// Significant changes.

if (!is_dir('articles')) {
	die('Aborted. You must first create an articles directory');
}

function show_table($tablename)
{
	global $db2;

	echo $tablename . '<br/>';
	$rec = $db2->query("SELECT * FROM $tablename ORDER BY id")->fetch_all();
	if ($rec !== FALSE) {
		echo table2html($rec);
	}
	else {
		echo "NO CONTENTS FOR $tablename TABLE.<br/>";
	}
	echo '<hr/>';

}

echo 'Converting from version 1.6 to 2.0<br/>';

/* =============================================================
 * Part 1
 * Using the posts table to grab post files and rewrite them
 * into the articles/ directory in their new format
 * ============================================================= */

echo 'PART ONE: converting existing posts/excerpts to new format<br/>';

include 'includes/table2html.inc.php';
include 'libraries/database.lib.php';
$db = new database(array('dbdriv' => 'SQLite3', 'dbdata' => 'blog.dta'));

$sql = "SELECT * FROM posts";
$posts = $db->query($sql)->fetch_all();
$sql = "SELECT * FROM user";
$users = $db->query($sql)->fetch_all();
$sql = "SELECT * FROM topics";
$topics = $db->query($sql)->fetch_all();
$sql = "SELECT postno, abbrev, topicid, name FROM post2topics, topics WHERE topics.id = post2topics.topicid";
$p2t = $db->query($sql)->fetch_all();

foreach ($posts as $post) {
	// grab content
	$content = file_get_contents('posts/' . $post['filename']);
	// grab teaser/excerpt
	$teaser = file_get_contents('excerpts/' . $post['filename']);
	// get author
	foreach ($users as $user) {
		if ($post['author'] === $user['id']) {
			$login = $user['login'];
			break;
		}
	}

	// get topics
	$t = array();
	foreach ($p2t as $p) {
		if ($p['postno'] === $post['id']) {
			$t[] = $p['abbrev'];
		}
	}
	$tops = implode(',', $t);

	$newfile = '@title ' . $post['title'] . PHP_EOL;
	$newfile .= '@author ' . $login . PHP_EOL;
	$newfile .= '@markup ' . 'html' . PHP_EOL;
	$newfile .= '@timestamp ' . $post['timestamp'] . PHP_EOL;

	if ($post['comments_allowed']) {
		$newfile .= '@comments' . PHP_EOL;
	}
	
	$newfile .= '@topics ' . $tops . PHP_EOL;

	$newfile .= '@teaser' . PHP_EOL;
	$newfile .= $teaser . PHP_EOL;
	$newfile .= '@content' . PHP_EOL;
	$newfile .= $content . PHP_EOL;

	file_put_contents('articles/' . $post['filename'], $newfile);
}

echo 'PART ONE completed. Post/excerpts converted to new format<br/>';

/* =============================================================
 * Part 2
 * Refactor existing tables and use them to create a new
 * database file called blog.sq3.
 * ============================================================= */

echo 'PART TWO: refactoring database<br/>';

if (file_exists('blog.sq3')) {
	unlink('blog.sq3');
}

$db2 = new database(array('dbdriv' => 'SQLite3', 'dbdata' => 'blog.sq3'));

// periods table

// Change table name from archive_stats to periods.
// All field definitions are the same

$sql = 'CREATE TABLE periods (id integer primary key autoincrement, period varchar(6), qty integer)';
$db2->query($sql);

$sql = 'SELECT * FROM archive_stats';
$recs = $db->query($sql)->fetch_all();
if ($recs !== FALSE) {
	foreach ($recs as $rec) {
		$db2->insert('periods', $rec);
	}
}

// topics table

// no changes

$sql = 'CREATE TABLE topics (id integer primary key autoincrement, abbrev varchar(10), name varchar(50), qty integer default 0)';
$db2->query($sql);
foreach ($topics as $topic) {
	$db2->insert('topics', $topic);
}

// confirm table

// No changes to this table (part of the "user" infrastructure)

$sql = 'CREATE TABLE confirm (id integer primary key autoincrement, login varchar(30), password varchar(255), name varchar(50), email varchar(255), nonce varchar(255), level integer, ip varchar(15), link varchar(255), timestamp integer)';
$db2->query($sql);

$sql = 'SELECT * FROM confirm';
$recs = $db->query($sql)->fetch_all();
if ($recs !== FALSE) {
	foreach ($recs as $rec) {
		$db2->insert('confirm', $rec);
	}
}

// user table

// No changes to this table (part of the "user" infrastructure)

$sql = 'CREATE TABLE user (id integer primary key autoincrement, login varchar(30), password varchar(255), name varchar(50) not null, email varchar(255) not null, nonce varchar(255) not null, level integer default 255)';
$db2->query($sql);

$sql = 'SELECT * FROM user';
$recs = $db->query($sql)->fetch_all();
if ($recs !== FALSE) {
	foreach ($recs as $rec) {
		$db2->insert('user', $rec);
	}
}

// comments table

// No changes to this table

$sql = 'CREATE TABLE comments (id integer primary key autoincrement, filename varchar(255) not null, artno integer references articles(id), author integer references user(id), status varchar(4), timestamp varchar(14) not null, email varchar(255), name varchar(50))';
$db2->query($sql);

$sql = 'SELECT * FROM comments';
$recs = $db->query($sql)->fetch_all();
if ($recs !== FALSE) {
	foreach ($recs as $rec) {
		$ins = array(
			'id' => $rec['id'],
			'filename' => $rec['filename'],
			'artno' => $rec['postno'],
			'author' => $rec['author'],
			'status' => $rec['status'],
			'timestamp' => $rec['timestamp'],
			'email' => $rec['email'],
			'name' => $rec['name']
		);

		$db2->insert('comments', $ins);
	}
}

// post2topics table    

// Table name changed to art2topics

$sql = 'CREATE TABLE art2topics (id integer primary key autoincrement, artno integer references articles(id), topicid integer references topic(id))';
$db2->query($sql);
$sql = 'SELECT * FROM post2topics';
$recs = $db->query($sql)->fetch_all();
foreach ($recs as $p) {
	$rec = array('id' => $p['id'],
		'artno' => $p['postno'],
		'topicid' => $p['topicid']
	);
	$db2->insert('art2topics', $rec);
}

// posts/articles table

// Table name changed to articles
// Fields omitted: parent, postid, revno, latest

$sql = 'CREATE TABLE articles (id integer primary key autoincrement, author integer references user(id), filename varchar(255) not null, timestamp varchar(14) not null, comments_allowed boolean default 0, title varchar(255) not null)';
$db2->query($sql);

foreach ($posts as $post) {
	$prec = $db2->prepare('articles', $post);
	$db2->insert('articles', $prec);
}

// teasers table

// This is a new table created to facilitate showing a page of teasers.
// Without this table, the system must scan all files for the teasers

$sql = 'CREATE TABLE teasers (id integer primary key autoincrement, artno integer not null references articles(id), text varchar(255) not null)';
$db2->query($sql);

foreach ($posts as $post) {
	$filename = 'excerpts/' . $post['filename'];
	$teaser = $db2->quote(file_get_contents($filename));
	$save = array('artno' => $post['id'], 'text' => $teaser);
	$db2->insert('teasers', $save);
}

echo 'PART TWO completed.<br/>';

show_table('user');
show_table('articles');
show_table('teasers');
show_table('topics');
show_table('art2topics');
show_table('periods');
show_table('confirm');


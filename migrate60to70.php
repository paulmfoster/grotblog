<?php

// This code reads the INI file for an article, and writes it to the top of
// an article file. It does *not* remove the INI files.

include 'init.php';

$cells = [];

function insert_ini($basename)
{
    global $cells;

    $found = FALSE;

    $max = count($cells);
    if ($max == 0) {
        $cells[0] = ['ini' => $basename, 'file' => '', 'ext' => ''];
        $found = TRUE;
    }
    else {
        for ($i = 0; $i < $max; $i++) {
            if ($cells[$i]['file'] == $basename) {
                $cells[$i]['ini'] = $basename;
                $found = TRUE;
                break;
            }
        } 
    }

    if (!$found)
        $cells[$max] = ['ini' => $basename, 'file' => '', 'ext' => ''];
}

function insert_file($basename, $ext)
{
    global $cells;

    $found = FALSE;

    $max = count($cells);
    if ($max == 0) {
        $cells[0] = ['ini' => '', 'file' => $basename, 'ext' => $ext];
        $found = TRUE;
    }
    else {
        for ($i = 0; $i < $max; $i++) {
            if ($cells[$i]['ini'] == $basename) {
                $cells[$i]['file'] = $basename;
                $cells[$i]['ext'] = $ext; 
                $found = TRUE;
                break;
            }
        }
    } 

    if (!$found)
        $cells[$max] = ['ini' => '', 'file' => $basename, 'ext' => $ext];

}

if ($hand = opendir(ARTSDIR)) {
    while ($file = readdir($hand)) {
        if ($file == '.' || $file == '..')
            continue;

        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $filename = pathinfo($file, PATHINFO_FILENAME);

        if ($ext == 'ini') {
            insert_ini($filename);
        }
        else {
            insert_file($filename, $ext);
        }
    }
    closedir($hand);
}

foreach ($cells as $cell) {

    // get ini data
    $inidata = file_get_contents(ARTSDIR . $cell['file'] . '.ini');

    // get article data
    $article = file_get_contents(ARTSDIR . $cell['file'] . '.' . $cell['ext']);

    // mash them together
    $all = '<!--' . PHP_EOL;
    $all .= $inidata;
    $all .= '-->' . PHP_EOL;
    $all .= $article;

    // write 'em
    file_put_contents(ARTSDIR . $cell['file'] . '.' . $cell['ext'], $all);
}


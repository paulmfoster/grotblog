<?php

include 'init.php';

$topicid = $_GET['id'] ?? NULL;
if (is_null($topicid))
    redirect(ADMIN_URL);

access(0, DEFAULT_URL);

$topics = model('topics', $db);

$topic = $topics->get_topic_from_id($topicid);	

$fields = array(
    'topicid' => array(
        'name' => 'topicid',
        'type' => 'hidden',
        'value' => $topicid
    ),
    'name' => array(
        'name' => 'name',
        'type' => 'text',
        'size' => 30,
        'maxlength' => 50,
        'required' => 1,
        'value' => $topic['name']
    ),
    's1' => array(
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Save'
    )
);

$form->set($fields);
$page_title = 'Edit Topics';
$return = 'edittop2.php';
$focus_field = 'name';
include VIEWDIR . 'edittop.view.php';


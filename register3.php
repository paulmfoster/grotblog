<?php

include 'init.php';

$token = $_POST['token'] ?? NULL;
if (is_null($token)) {
    redirect(DEFAULT_URL);
}

$okay = $user->confirm_registration($token);
if ($okay) {	
    emsg('S', 'Registration successfully completed');
}
redirect(DEFAULT_URL);

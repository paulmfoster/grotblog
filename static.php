<?php

include 'init.php';

$page = $_GET['page'] ?? NULL;

if (is_null($page)) {
    redirect(DEFAULT_URL);
}

if (!file_exists(VIEWDIR . $page)) {
    emsg('F', 'No such page exists');
    redirect(DEFAULT_URL);
}

$page_title = '';
include VIEWDIR . 'head.view.php';
include VIEWDIR . $page;
include VIEWDIR . 'footer.view.php';

